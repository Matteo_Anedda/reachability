# Workcell Repository

This repository contains packages from various sources as well as self-written packages intended for the automatic construction of multi-robot workcells.

## reachability

Bachelor thesis code.

## mtc 

Successor to the bachelor's thesis, realizing more complex cell designs and task executions.

## franka_description

Describes the robots. Must be expanded for each additional robot.

## ceti* packages

Robot configurations for Ceti-like robots consisting of a cube and an arm attached to it. The size of the cube and the position of the arm are calculated based on the given parameters.