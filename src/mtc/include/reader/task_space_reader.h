#ifndef TASK_SPACE_READER_
#define TASK_SPACE_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>

#include "reader/abstract_param_reader.h"


class Task_space_reader : public Abstract_param_reader{
    public:
    Task_space_reader(std::shared_ptr<ros::NodeHandle> const& d) : Abstract_param_reader(d){};
    
    void read() override;
};
#endif
