#ifndef ROBOT_READER_
#define ROBOT_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>

#include "reader/abstract_param_reader.h"


class Robot_reader : public Abstract_param_reader{
    protected:
    std::vector<object_data> robot_data_;

    public:
    Robot_reader(std::shared_ptr<ros::NodeHandle> const& d) : Abstract_param_reader(d){};
    
    inline void set_robot_data(std::vector<object_data>& robot_data) {robot_data_ = robot_data;}
    inline std::vector<object_data> robot_data() {return robot_data_;}

    void read() override;
};
#endif
