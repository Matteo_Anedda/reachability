#ifndef WING_READER_
#define WING_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>

#include "reader/abstract_param_reader.h"


class Wing_reader : public Abstract_param_reader{
    protected:
    std::vector<std::pair<std::vector<object_data>, int>> wing_data_;

    public:
    Wing_reader(std::shared_ptr<ros::NodeHandle> const& d) : Abstract_param_reader(d){};
    
    inline void set_wing_data(std::vector<std::pair<std::vector<object_data>, int>>& wing_data) {wing_data_ = wing_data;}
    inline std::vector<std::pair<std::vector<object_data>, int>> wing_data() {return wing_data_;}

    void read() override;
};
#endif
