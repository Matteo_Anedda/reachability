#ifndef ABSTRACT_PARAM_READER_
#define ABSTRACT_PARAM_READER_

#include "ros/ros.h"
#include <ros/package.h>
#include <xmlrpcpp/XmlRpc.h>
#include <tf2/LinearMath/Transform.h>
#include <yaml-cpp/yaml.h>
#include <fstream>
#include <regex>


struct object_data {
    std::string name_;
    tf2::Transform pose_;
    tf2::Vector3 size_;
};

class Abstract_param_reader{
    protected:
    std::shared_ptr<ros::NodeHandle> nh_;

    public:
    Abstract_param_reader(std::shared_ptr<ros::NodeHandle> const& d) : nh_(d){}

    float float_of(XmlRpc::XmlRpcValue& val);
    virtual void read()=0;
};
#endif
