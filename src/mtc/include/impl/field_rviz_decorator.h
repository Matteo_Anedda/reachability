#ifndef FIELD_RVIZ_DECORATOR_
#define FIELD_RVIZ_DECORATOR_

#include "ros/ros.h"
#include "visualization_msgs/Marker.h"
#include "impl/field.h"
#include "impl/abstract_robot_element_decorator.h"

class Field_rviz_decorator  : public Abstract_robot_element_decorator{
    private:
    visualization_msgs::MarkerArray* markers_;


    public:
    Field_rviz_decorator(Abstract_robot_element* next, visualization_msgs::MarkerArray* markers) : Abstract_robot_element_decorator(next), markers_(markers){};
    
    inline void set_markers(visualization_msgs::MarkerArray* markers) { markers_= markers;}
    inline visualization_msgs::MarkerArray* markers() { return markers_;}

    void update(tf2::Transform& tf) override;
    void input_filter(tf2::Transform& tf) override;
    void output_filter() override;
};


#endif