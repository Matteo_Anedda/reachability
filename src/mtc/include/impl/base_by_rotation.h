#ifndef BASE_BY_ROTATION_
#define BASE_BY_ROTATION_

#include <ros/ros.h>
#include "impl/abstract_map_loader.h"
#include "impl/abstract_strategy.h"


class Base_by_rotation : public Abstract_strategy {
    public:
    Base_by_rotation()= default;
    ~Base_by_rotation()= default;

    void inv_map_creation(Abstract_map_loader* var) override;
    void cloud_calculation(Abstract_map_loader* var) override;

    
};

#endif