#ifndef Field_
#define Field_

#include "ros/ros.h"
#include "impl/abstract_robot_element.h"

class Field  : public Abstract_robot_element{
    public:
    Field(tf2::Transform tf){ relative_tf_ = tf;}
    
    void update(tf2::Transform& tf) override {this->calc_world_tf(tf);}
};

#endif