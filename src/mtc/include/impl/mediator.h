#ifndef MEDIATOR_
#define MEDIATOR_

#include <ros/ros.h>
#include <ros/package.h>
#include <yaml-cpp/yaml.h>
#include <fstream>
#include "impl/abstract_mediator.h"
#include "impl/abstract_robot.h"
#include "impl/robot.h"

class Mediator : public Abstract_mediator{
    public:
    Mediator(std::vector<std::vector<tf2::Transform>> objects, ros::Publisher* pub) : Abstract_mediator(objects, pub){};

    pcl::PointCloud< pcl::PointXYZ >::Ptr vector_to_cloud(std::vector<pcl::PointXYZ>& vector);
    std::vector<pcl::PointXYZ> generate_Ground(const tf2::Vector3 origin, const float diameter, float resolution);
    void approximation(Robot* robot);
    void write_file(Robot* A, Robot* B);
    void publish(Robot* r);

    void setup_rviz();
    void calculate(std::vector<tf2::Transform>& ground_per_robot);
    
    void set_wings(std::vector<std::pair<std::vector<object_data>, int>>& wbp) override;
    bool check_collision(const int& robot) override;
    void mediate() override;
    void build_wings(std::bitset<3>& wings, int& robot) override;

};

#endif