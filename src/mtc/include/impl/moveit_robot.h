#ifndef MOVEIT_ROBOT_
#define MOVEIT_ROBOT_

#include <ros/ros.h>
#include "impl/robot.h"


class Moveit_robot : public Robot{
    protected:
    moveit::planning_interface::MoveGroupInterface* mgi_;
    std::map<std::string, std::string>  map_; 




    public:
    Moveit_robot(std::string& name, tf2::Transform tf, tf2::Vector3 size) : Robot(name, tf, size){
        mgi_ = new moveit::planning_interface::MoveGroupInterface(name);
        std::stringstream hand_n, ik_frame_n, name_n;
        hand_n << "hand_" << name.back();
        ik_frame_n << "panda_" << name.back() << "_link8";
        auto mgi_hand = moveit::planning_interface::MoveGroupInterface(hand_n.str());

        map_.insert(std::make_pair<std::string, std::string>("right_finger", mgi_hand.getLinkNames()[0].c_str()));
        map_.insert(std::make_pair<std::string, std::string>("left_finger", mgi_hand.getLinkNames()[1].c_str()));
        map_.insert(std::make_pair<std::string, std::string>("hand_link", mgi_hand.getLinkNames()[2].c_str()));
        map_.insert(std::make_pair<std::string, std::string>("eef_name", hand_n.str()));
        map_.insert(std::make_pair<std::string, std::string>("hand_frame", ik_frame_n.str()));
        map_.insert(std::make_pair<std::string, std::string>("hand_group_name", hand_n.str()));
    }

    inline moveit::planning_interface::MoveGroupInterface* mgi() {return mgi_;}
    inline std::map<std::string, std::string>&  map(){return map_;};


};

#endif