#ifndef WING_
#define WING_

#include "ros/ros.h"
#include "impl/abstract_robot_element.h"

class Wing  : public Abstract_robot_element{
    protected:
    std::string name_;
    tf2::Vector3 size_;
    std::vector<tf2::Transform> bounds_;

    public:
    Wing(std::string name, tf2::Transform tf, tf2::Vector3 size) : size_(size), name_(name){ 
        relative_tf_ = tf;
        bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(size_.getX()/2.f, size_.getY()/2.f,0))); //++
        bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(size_.getX()/2.f, size_.getY()/-2.f,0))); //+-
        bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(size_.getX()/-2.f, size_.getY()/-2.f,0))); //--
        bounds_.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(size_.getX()/-2.f, size_.getY()/2.f,0))); //-+
        }
    virtual ~Wing() {}
        
    inline std::string& name() {return name_;}
    inline void set_name(std::string str) {name_ = str;}
    inline void set_set(tf2::Vector3& vec) {size_ = vec;}

    inline tf2::Vector3 size(){ return size_;}
    inline std::vector<tf2::Transform>& bounds() {return bounds_;}
    void update(tf2::Transform& tf) override {this->calc_world_tf(tf);}
};

#endif