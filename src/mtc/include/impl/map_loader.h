#ifndef MAP_LOADER_
#define MAP_LOADER_

#include "ros/ros.h"
#include "impl/abstract_map_loader.h"
#include <regex>



class Map_loader : public Abstract_map_loader {
    public:
    Map_loader(XmlRpc::XmlRpcValue& map_data, XmlRpc::XmlRpcValue& target_data);
    std::vector<std::vector<pcl::PointXYZ>> base_calculation() override;
    void write_task(Abstract_robot* robot) override;

};
#endif