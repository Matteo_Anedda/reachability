#ifndef WING_RVIZ_DECORATOR_
#define WING_RVIZ_DECORATOR_

#include "ros/ros.h"
#include "visualization_msgs/Marker.h"
#include "impl/wing.h"
#include "impl/abstract_robot_element_decorator.h"

class Wing_rviz_decorator  : public Abstract_robot_element_decorator{
    private:
    visualization_msgs::Marker* marker_ = new visualization_msgs::Marker();


    public:
    Wing_rviz_decorator(Abstract_robot_element* next) : Abstract_robot_element_decorator(next){};
    inline void set_markers(visualization_msgs::Marker* markers) { marker_= markers;}
    inline visualization_msgs::Marker* markers() { return marker_;}
    void update(tf2::Transform& tf) override;
    void input_filter(tf2::Transform& tf) override;
    void output_filter() override;
};


#endif