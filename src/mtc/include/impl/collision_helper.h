#ifndef COLLISION_HELPER_
#define COLLISION_HELPER_

#include <ros/ros.h>
#include <moveit/collision_detection/collision_env.h>
#include <moveit/collision_detection_fcl/collision_env_fcl.h>



class Collision_helper : public collision_detection::CollisionEnvFCL{
    public:
    Collision_helper()= default;
    ~Collision_helper()= default;

    bool collision_detector(const std::string& object1_name, const std::string& object2_name);
};

#endif