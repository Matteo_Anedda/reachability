#ifndef ABSTRACT_STRATEGY_
#define ABSTRACT_STRATEGY_

#include <ros/ros.h>
#include "impl/abstract_map_loader.h"
class Abstract_map_loader;

class Abstract_strategy {
    public:
    Abstract_strategy() = default;
    ~Abstract_strategy() = default;
    virtual void inv_map_creation(Abstract_map_loader* var)=0;
    virtual void cloud_calculation(Abstract_map_loader* var)=0;

};
#endif