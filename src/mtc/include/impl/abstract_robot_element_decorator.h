#ifndef ABSTRACT_ROBOT_ELEMENT_DECORATOR_
#define ABSTRACT_ROBOT_ELEMENT_DECORATOR_

#include "ros/ros.h"
#include "impl/abstract_robot_element.h"

class Abstract_robot_element_decorator  : public Abstract_robot_element{
    protected:
    Abstract_robot_element* next_;


    public:
    Abstract_robot_element_decorator(Abstract_robot_element* next) : next_(next){};
    
    inline Abstract_robot_element* wing() { return next_;}

    void update(tf2::Transform& tf) override {next_->update(tf);}
    virtual void input_filter(tf2::Transform& tf)=0;
    virtual void output_filter()=0;
};


#endif