#ifndef WING_MOVEIT_DECORATOR_
#define WING_MOVEIT_DECORATOR_

#include <ros/ros.h>
#include "impl/wing.h"
#include "impl/abstract_robot_element_decorator.h"

#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <moveit/planning_scene/planning_scene.h>

class Wing_moveit_decorator  : public Abstract_robot_element_decorator{
    private:
    moveit_msgs::CollisionObject* markers_ = new moveit_msgs::CollisionObject();


    public:
    Wing_moveit_decorator(Abstract_robot_element* next) : Abstract_robot_element_decorator(next){};
    inline void set_markers(moveit_msgs::CollisionObject* markers) { markers_= markers;}
    inline moveit_msgs::CollisionObject* markers() { return markers_;}
    
    void update(tf2::Transform& tf) override;
    void input_filter(tf2::Transform& tf) override;
    void output_filter() override;
};


#endif