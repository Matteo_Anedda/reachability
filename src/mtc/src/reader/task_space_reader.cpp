#include "reader/task_space_reader.h"


void Task_space_reader::read(){
    std::string filename = "dummy.yaml";
    //nh_->getParam("/resource_name", filename);

    XmlRpc::XmlRpcValue resources;
    nh_->getParam("/objects", resources);

    std::vector<tf2::Vector3> robot_middle;
    std::vector<tf2::Vector3>* robot_pointer = nullptr;


    std::regex rx("table([0-9]+)_table_top");
    std::smatch match;

    ROS_INFO("--- TASK_SPACE_READER ---");
    std::map<const std::string, std::vector<tf2::Vector3>> map;

    for(int i = 0; i < resources.size(); i++){
      std::string str = resources[i]["id"];
      if (std::regex_match(str, match, rx)){
        std::stringstream ss;
        ss << "panda_arm" << match[1].str().c_str();
        map.insert(std::make_pair<const std::string, std::vector<tf2::Vector3>>(ss.str().c_str(), std::vector<tf2::Vector3>()));
      }
    }

    tf2::Vector3 pos(0,0,0);
    for (int i = 0; i < resources.size(); i++){
      if (resources[i]["type"] == "DROP_OFF_LOCATION"){
        std::string str = resources[i]["id"];
        if(str[0] == 'A')  robot_pointer = &map.at("panda_arm1");
        if(str[0] == 'B')  robot_pointer = &map.at("panda_arm1");
        if(str[0] == 'C')  robot_pointer = &map.at("panda_arm1");
        if(str[0] == 'D')  robot_pointer = &map.at("panda_arm1");
        if(str[0] == 'E')  robot_pointer = &robot_middle;
        if(str[0] == 'F')  robot_pointer = &robot_middle;
        if(str[0] == 'G')  robot_pointer = &map.at("panda_arm2");
        if(str[0] == 'H')  robot_pointer = &map.at("panda_arm2");
        if(str[0] == 'I')  robot_pointer = &map.at("panda_arm2");
        if(str[0] == 'J')  robot_pointer = &map.at("panda_arm2");
      
        if (robot_pointer){
          tf2::Vector3 pos(0,0,0);
          (resources[i]["pos"].hasMember("x")) ? pos.setX(float_of(resources[i]["pos"]["x"])) :pos.setX(0);
          (resources[i]["pos"].hasMember("y")) ? pos.setY(float_of(resources[i]["pos"]["y"])) :pos.setY(0);
          (resources[i]["pos"].hasMember("z")) ? pos.setZ(float_of(resources[i]["pos"]["z"])) :pos.setZ(0);
          robot_pointer->push_back(pos);
        }
      }
      robot_pointer = nullptr;
    }

    // lets make some result data
    ROS_INFO("writing file...");
    std::ofstream o(ros::package::getPath("mtc") + "/descriptions/" + filename);
    YAML::Node node;

    for(auto robot: map){
      for(tf2::Vector3& vec : robot.second){
        node["task"]["groups"][robot.first].push_back(YAML::Load(std::string("{ 'pos': {'x': ") + std::to_string(vec.getX()) + ", 'y': " + std::to_string(vec.getY()) + ", 'z':" + std::to_string(vec.getZ()) + std::string("}}")));
      }
    }

    for(auto robot: map){
      for(tf2::Vector3& vec : robot_middle){
        node["task"]["groups"][robot.first].push_back(YAML::Load(std::string("{ 'pos': {'x': ") + std::to_string(vec.getX()) + ", 'y': " + std::to_string(vec.getY()) + ", 'z':" + std::to_string(vec.getZ()) + std::string("}}")));
      }
    }
    o << node;
    o.close();
}
