#include "reader/abstract_param_reader.h"

float Abstract_param_reader::float_of(XmlRpc::XmlRpcValue& val){
    float t = 0;
    try{
        switch (val.getType()){
            case XmlRpc::XmlRpcValue::TypeInt:
                t = static_cast<int>(val);
                break;
            case XmlRpc::XmlRpcValue::TypeDouble:
                t = static_cast<double>(val);
                break;
            case XmlRpc::XmlRpcValue::TypeString:
                t = std::stof(val);
                break;
            default:
                ROS_INFO("value type is some unknown type");
            }
    } catch (XmlRpc::XmlRpcException){
        ROS_INFO("Something went wrong, returning 0");
    }
    return t;
}