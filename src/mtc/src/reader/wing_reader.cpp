#include "reader/wing_reader.h"

void Wing_reader::read(){
    XmlRpc::XmlRpcValue resources;
    nh_->getParam("/objects", resources);

    std::regex rx("table([0-9]+)_table_top");
    std::smatch match;

    ROS_INFO("--- WING_READER ---");
    for(int i = 0; i < resources.size(); i++){
        std::string str = resources[i]["id"];
        if(std::regex_match(str, match, rx)){
            std::vector<object_data> wd;
            wd.push_back({"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0)), tf2::Vector3(0,0,0)});
            wd.push_back({"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0)), tf2::Vector3(0,0,0)});
            wd.push_back({"", tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0)), tf2::Vector3(0,0,0)});
            std::pair<std::vector<object_data>, int> pair;
            pair.first = wd;
            pair.second = 0;
            wing_data_.push_back(pair);
        }
    }


    rx.assign("table([0-9]+)_right_panel");
    for(int i = 0; i < resources.size(); i++){
      std::string str = resources[i]["id"];
      if (std::regex_match(str, match, rx)){
        ROS_INFO("=> WING: description found, loading...");
        tf2::Vector3 pos;
        tf2::Vector3 size;
        tf2::Quaternion rot;

        
        (resources[i]["size"].hasMember("length")) ? size.setX(float_of(resources[i]["size"]["length"])) :size.setX(0);
        (resources[i]["size"].hasMember("width")) ? size.setY(float_of(resources[i]["size"]["width"])) :size.setY(0);
        (resources[i]["size"].hasMember("height")) ? size.setZ(float_of(resources[i]["size"]["height"])) :size.setZ(0);

        (resources[i]["pos"].hasMember("x")) ? pos.setX(float_of(resources[i]["pos"]["x"])) :pos.setX(0);
        (resources[i]["pos"].hasMember("y")) ? pos.setY(float_of(resources[i]["pos"]["y"])) :pos.setY(0);
        (resources[i]["pos"].hasMember("z")) ? pos.setZ(float_of(resources[i]["pos"]["z"]) + size.getZ()/2) :pos.setZ(0);
        (resources[i]["orientation"].hasMember("x")) ? rot.setX(float_of(resources[i]["orientation"]["x"])) :rot.setX(0);
        (resources[i]["orientation"].hasMember("y")) ? rot.setY(float_of(resources[i]["orientation"]["y"])) :rot.setY(0);
        (resources[i]["orientation"].hasMember("z")) ? rot.setZ(float_of(resources[i]["orientation"]["z"])) :rot.setZ(0);
        (resources[i]["orientation"].hasMember("w")) ? rot.setW(float_of(resources[i]["orientation"]["w"])) :rot.setW(0);

        ROS_INFO("--- Wing %s ---", str.c_str());
        ROS_INFO("=> WING: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
        ROS_INFO("=> WING: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
        ROS_INFO("=> WING: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());


        wing_data_[std::atoi(match[1].str().c_str())-1].first[0].name_ = str.c_str();
        wing_data_[std::atoi(match[1].str().c_str())-1].first[0].pose_ = tf2::Transform(rot.normalized(), pos); // mediator->robots()[0]->tf().inverse() *
        wing_data_[std::atoi(match[1].str().c_str())-1].first[0].size_ = size;
        wing_data_[std::atoi(match[1].str().c_str())-1].second += std::pow(2, 0);
      }
    }

    rx.assign("table([0-9]+)_front_panel");
    for(int i = 0; i < resources.size(); i++){
      std::string str = resources[i]["id"];
      if (std::regex_match(str, match, rx)){
        ROS_INFO("=> WING: description found, loading...");
        tf2::Vector3 pos;
        tf2::Vector3 size;
        tf2::Quaternion rot;

        
        (resources[i]["size"].hasMember("length")) ? size.setX(float_of(resources[i]["size"]["length"])) :size.setX(0);
        (resources[i]["size"].hasMember("width")) ? size.setY(float_of(resources[i]["size"]["width"])) :size.setY(0);
        (resources[i]["size"].hasMember("height")) ? size.setZ(float_of(resources[i]["size"]["height"])) :size.setZ(0);

        (resources[i]["pos"].hasMember("x")) ? pos.setX(float_of(resources[i]["pos"]["x"])) :pos.setX(0);
        (resources[i]["pos"].hasMember("y")) ? pos.setY(float_of(resources[i]["pos"]["y"])) :pos.setY(0);
        (resources[i]["pos"].hasMember("z")) ? pos.setZ(float_of(resources[i]["pos"]["z"]) + size.getZ()/2) :pos.setZ(0);
        (resources[i]["orientation"].hasMember("x")) ? rot.setX(float_of(resources[i]["orientation"]["x"])) :rot.setX(0);
        (resources[i]["orientation"].hasMember("y")) ? rot.setY(float_of(resources[i]["orientation"]["y"])) :rot.setY(0);
        (resources[i]["orientation"].hasMember("z")) ? rot.setZ(float_of(resources[i]["orientation"]["z"])) :rot.setZ(0);
        (resources[i]["orientation"].hasMember("w")) ? rot.setW(float_of(resources[i]["orientation"]["w"])) :rot.setW(0);

        ROS_INFO("--- Wing %s ---", str.c_str());
        ROS_INFO("=> WING: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
        ROS_INFO("=> WING: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
        ROS_INFO("=> WING: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());


        wing_data_[std::atoi(match[1].str().c_str())-1].first[1].name_ = str.c_str();
        wing_data_[std::atoi(match[1].str().c_str())-1].first[1].pose_ = tf2::Transform(rot.normalized(), pos); // mediator->robots()[0]->tf().inverse() *
        wing_data_[std::atoi(match[1].str().c_str())-1].first[1].size_ = size;
        wing_data_[std::atoi(match[1].str().c_str())-1].second += std::pow(2, 1);


      }
    }

    rx.assign("table([0-9]+)_left_panel");
    for(int i = 0; i < resources.size(); i++){
      std::string str = resources[i]["id"];
      if (std::regex_match(str, match, rx)){
        ROS_INFO("=> WING: description found, loading...");
        tf2::Vector3 pos;
        tf2::Vector3 size;
        tf2::Quaternion rot;

        
        (resources[i]["size"].hasMember("length")) ? size.setX(float_of(resources[i]["size"]["length"])) :size.setX(0);
        (resources[i]["size"].hasMember("width")) ? size.setY(float_of(resources[i]["size"]["width"])) :size.setY(0);
        (resources[i]["size"].hasMember("height")) ? size.setZ(float_of(resources[i]["size"]["height"])) :size.setZ(0);

        (resources[i]["pos"].hasMember("x")) ? pos.setX(float_of(resources[i]["pos"]["x"])) :pos.setX(0);
        (resources[i]["pos"].hasMember("y")) ? pos.setY(float_of(resources[i]["pos"]["y"])) :pos.setY(0);
        (resources[i]["pos"].hasMember("z")) ? pos.setZ(float_of(resources[i]["pos"]["z"]) + size.getZ()/2) :pos.setZ(0);
        (resources[i]["orientation"].hasMember("x")) ? rot.setX(float_of(resources[i]["orientation"]["x"])) :rot.setX(0);
        (resources[i]["orientation"].hasMember("y")) ? rot.setY(float_of(resources[i]["orientation"]["y"])) :rot.setY(0);
        (resources[i]["orientation"].hasMember("z")) ? rot.setZ(float_of(resources[i]["orientation"]["z"])) :rot.setZ(0);
        (resources[i]["orientation"].hasMember("w")) ? rot.setW(float_of(resources[i]["orientation"]["w"])) :rot.setW(0);

        ROS_INFO("--- Wing %s ---", str.c_str());
        ROS_INFO("=> WING: pos('%f', '%f', '%f')", pos.getX(), pos.getY(), pos.getZ());
        ROS_INFO("=> WING: orientation('%f', '%f', '%f', '%f')", rot.getX(), rot.getY(), rot.getZ(), rot.getW());
        ROS_INFO("=> WING: size('%f', '%f', '%f')", size.getX(), size.getY(), size.getZ());


        wing_data_[std::atoi(match[1].str().c_str())-1].first[2].name_ = str.c_str();
        wing_data_[std::atoi(match[1].str().c_str())-1].first[2].pose_ = tf2::Transform(rot.normalized(), pos); // mediator->robots()[0]->tf().inverse() *
        wing_data_[std::atoi(match[1].str().c_str())-1].first[2].size_ = size;
        wing_data_[std::atoi(match[1].str().c_str())-1].second += std::pow(2, 2);

      }
    }
}
