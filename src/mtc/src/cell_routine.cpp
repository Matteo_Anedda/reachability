#include "impl/abstract_robot.h"
#include "impl/abstract_map_loader.h"
#include "impl/map_loader.h"
#include "impl/abstract_robot_element.h"
#include "impl/abstract_robot_element_decorator.h"
#include "impl/abstract_mediator.h"
#include "impl/moveit_mediator.h"
#include "impl/wing_moveit_decorator.h"
#include "impl/wing.h"
#include "impl/moveit_robot.h"
#include "impl/collision_helper.h"
#include <xmlrpcpp/XmlRpc.h>
#include "reader/abstract_param_reader.h"
#include "reader/robot_reader.h"
#include "reader/wing_reader.h"


int main(int argc, char **argv){
    ros::init(argc, argv, "cell_routine");
    std::shared_ptr<ros::NodeHandle> nnh = std::make_shared<ros::NodeHandle>();
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    XmlRpc::XmlRpcValue map, task, resources;

    nnh->getParam("/objects",resources);
    nnh->getParam("/data",map);
    nnh->getParam("/task/groups",task);

    
    Abstract_map_loader* map_loader = new Map_loader(map, task);

          
    ros::Publisher* pub = new ros::Publisher(nnh->advertise< visualization_msgs::MarkerArray >("visualization_marker_array", 1)); //refractor

    Abstract_mediator* mediator = new Moveit_mediator(map_loader->task_grasps(), pub, nh);
    Moveit_mediator* moveit_mediator = dynamic_cast<Moveit_mediator*>(mediator);

    moveit_mediator->load_robot_description();

    std::unique_ptr<Abstract_param_reader> robot_reader(new Robot_reader(nnh));
    robot_reader->read();
    std::vector<object_data> rd = static_cast<Robot_reader*>(robot_reader.get())->robot_data();
    for (int i = 0; i < rd.size() ;i++) mediator->connect_robots(new Moveit_robot(rd[i].name_, rd[i].pose_, rd[i].size_));

    std::unique_ptr<Abstract_param_reader> wing_reader(new Wing_reader(nnh));
    wing_reader->read();
    auto wd = static_cast<Wing_reader*>(wing_reader.get())->wing_data();

    for (int i = 0; i < mediator->robots().size(); i++){
        for(object_data& w : wd[i].first){
            w.pose_ = mediator->robots()[i]->tf().inverse() * w.pose_;
        }
        mediator->robots()[i]->set_observer_mask(static_cast<wing_config>(wd[i].second));
    }


    moveit_mediator->set_wings(wd);
    Moveit_robot* ceti1 = dynamic_cast<Moveit_robot*>(mediator->robots()[0]);
    Moveit_robot* ceti2 = dynamic_cast<Moveit_robot*>(mediator->robots()[1]);
    

    for (int i = 0; i < mediator->robots().size(); i++){
        std::bitset<3> bs = wd[i].second;
        moveit_mediator->build_wings(bs, i);
    }


    //moveit::planning_interface::PlanningSceneInterface::applyCollisionObject()
    ceti1->notify();
    ceti2->notify();
    moveit_mediator->mediate();


    while (ros::ok()){
        ros::spinOnce();
    }
}