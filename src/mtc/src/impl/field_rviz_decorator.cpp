#include "impl/field_rviz_decorator.h"

void Field_rviz_decorator::update(tf2::Transform& tf) {
    Abstract_robot_element_decorator::update(tf);
    output_filter();
}

void Field_rviz_decorator::input_filter(tf2::Transform& tf) {}

void Field_rviz_decorator::output_filter() {
    Field* field = dynamic_cast<Field*> (next_);
    tf2::Vector3 world_origin = field->world_tf().getOrigin();
    tf2::Quaternion world_quat = field->world_tf().getRotation().normalized();

    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time();
    marker.ns = "fiels";
    marker.id = *((int*)(&next_));
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = world_origin.getX();
    marker.pose.position.y = world_origin.getY();
    marker.pose.position.z = 0;
    marker.pose.orientation.x = world_quat.getX();
    marker.pose.orientation.y = world_quat.getY();
    marker.pose.orientation.z = world_quat.getZ();
    marker.pose.orientation.w = world_quat.getW();
    marker.scale.x = 0.4f;
    marker.scale.y = 0.4f;
    marker.scale.z = 0.01f;
    marker.color.r = 0.7;
    marker.color.g = 0.7;
    marker.color.b = 0.7;
    marker.color.a = 1.0; // Don't forget to set the alpha!

    markers_->markers.push_back(marker);
}