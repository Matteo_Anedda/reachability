#include "impl/base_by_rotation.h"


void Base_by_rotation::inv_map_creation(Abstract_map_loader* var) {
    ROS_INFO("starting base calculation strategy...");
    ROS_INFO("condition based [inv_map] calculation...");
    std::vector<tf2::Transform> trans;
    for(long unsigned int i = 0; i < var->map().size(); i++) {
        for (long unsigned int x = 0; x < var->target_rot().size(); x++) {
            for (long unsigned int y = 0; y < var->target_rot()[x].size(); y++) {
                for(long unsigned int p = 0; p < var->target_rot()[x][y].size(); p++){
                    if (var->target_rot()[x][y][p].angle(var->map()[i].getRotation()) < 0.349066f) {
                        trans.push_back(tf2::Transform(var->target_rot()[x][y][p], var->map()[i].getOrigin()).inverse());
                        break;
                    }                        
                }
            }
        }
    }
    var->set_inv_map(trans);
    ROS_INFO("caculated [inv_map] contains %li entrys...", var->inv_map().size());
};

void Base_by_rotation::cloud_calculation(Abstract_map_loader* var) {
    ROS_INFO("initialyze thread implementations...");

    // Optimize targets in later implementation
    std::vector<std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>> target_clouds;
    target_clouds.resize(var->task_grasps().size());
    for(long unsigned int i = 0; i < target_clouds.size(); i++) {
        for (long unsigned int j = 0; j < var->task_grasps()[i].size();j++) {
            target_clouds[i].push_back(pcl::PointCloud< pcl::PointXYZ >::Ptr(new pcl::PointCloud< pcl::PointXYZ >));
        }
    }

    // Maybe OpenMP
    ROS_INFO("start [cloud] calculation...");
    tf2::Transform root(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0,0.4425f));
    for (long unsigned int i = 0; i < target_clouds.size(); i++){
        for (long unsigned int j = 0; j < target_clouds[i].size(); j++) {
            for (long unsigned int y = 0; y < var->inv_map().size(); y++) {
                for (long unsigned int x = 0; x < var->target_rot()[i][j].size(); x++) {
                    tf2::Transform target = var->task_grasps()[i][j]; target.setRotation(var->target_rot()[i][j][x]);
                    tf2::Transform base = target * (var->inv_map()[y] * root);
                    target_clouds[i][j]->push_back(pcl::PointXYZ(base.getOrigin().getX(), base.getOrigin().getY(), base.getOrigin().getZ()));
                }
            }
        }
    }
    var->set_target_cloud(target_clouds);
    ROS_INFO("[cloud]calculation done...");
}