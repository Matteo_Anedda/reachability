#include "impl/wing_rviz_decorator.h"

void Wing_rviz_decorator::update(tf2::Transform& tf) {
    //input_filter(tf);
    Abstract_robot_element_decorator::update(tf);
    output_filter();
}

void Wing_rviz_decorator::input_filter(tf2::Transform& tf) {
    tf2::Vector3 world_origin = tf.getOrigin();
    tf2::Quaternion world_quat = tf.getRotation().normalized();

    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time();
    marker.ns = "";
    marker.id = 1;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = world_origin.getX();
    marker.pose.position.y = world_origin.getY();
    marker.pose.position.z = world_origin.getZ();
    marker.pose.orientation.x = world_quat.getX();
    marker.pose.orientation.y = world_quat.getY();
    marker.pose.orientation.z = world_quat.getZ();
    marker.pose.orientation.w = world_quat.getW();
    marker.scale.x = 0.8f;
    marker.scale.y = 0.8f;
    marker.scale.z = 0.885f;
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    marker.color.a = 1.0; // Don't forget to set the alpha
}

void Wing_rviz_decorator::output_filter() {
    Wing* wing = dynamic_cast<Wing*> (next_);
    tf2::Vector3 world_origin = wing->world_tf().getOrigin();
    tf2::Quaternion world_quat = wing->world_tf().getRotation().normalized();

    marker_->header.frame_id = "map";
    marker_->header.stamp = ros::Time();
    marker_->ns = wing->name();
    marker_->id = 1;
    marker_->type = visualization_msgs::Marker::CUBE;
    marker_->action = visualization_msgs::Marker::MODIFY;
    marker_->pose.position.x = world_origin.getX();
    marker_->pose.position.y = world_origin.getY();
    marker_->pose.position.z = world_origin.getZ();
    marker_->pose.orientation.x = world_quat.getX();
    marker_->pose.orientation.y = world_quat.getY();
    marker_->pose.orientation.z = world_quat.getZ();
    marker_->pose.orientation.w = world_quat.getW();
    marker_->scale.x = wing->size().getX();
    marker_->scale.y = wing->size().getY();
    marker_->scale.z = wing->size().getZ();
    marker_->color.r = 1.0;
    marker_->color.g = 1.0;
    marker_->color.b = 1.0;
    marker_->color.a = 1.0; // Don't forget to set the alpha!
    /*
    visualization_msgs::Marker bounds;
    bounds.header.frame_id = "map";
    bounds.header.stamp = ros::Time();
    bounds.ns = "bounds";
    bounds.id = *((int*)(&wing));
    bounds.type = visualization_msgs::Marker::POINTS;
    bounds.action = visualization_msgs::Marker::MODIFY;
    bounds.scale.x = 0.01f;
    bounds.scale.y = 0.01f;
    bounds.scale.z = 0.01f;
    bounds.color.r = 0.0;
    bounds.color.g = 0.0;
    bounds.color.b = 1.0;
    bounds.color.a = 1.0;
    bounds.pose.orientation.w = 1;

    for (long unsigned int i = 0; i < wing->bounds().size(); i++){
        tf2::Transform point_posistion = wing->world_tf() * wing->bounds()[i];
        geometry_msgs::Point point;
        point.x = point_posistion.getOrigin().getX();
        point.y = point_posistion.getOrigin().getY();
        point.z = point_posistion.getOrigin().getZ();
        bounds.points.push_back(point);
    }
    */
}

