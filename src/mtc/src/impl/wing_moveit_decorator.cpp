#include "impl/wing_moveit_decorator.h"

void Wing_moveit_decorator::update(tf2::Transform& tf) {
    Abstract_robot_element_decorator::update(tf);
    output_filter();
}

void Wing_moveit_decorator::input_filter(tf2::Transform& tf) {}

void Wing_moveit_decorator::output_filter() {
    Wing* wing = dynamic_cast<Wing*> (next_);
    tf2::Vector3 world_origin = wing->world_tf().getOrigin();
    tf2::Quaternion world_quat = wing->world_tf().getRotation().normalized();


    markers_->id = wing->name();
    markers_->header.frame_id = "world";

    markers_->primitives.resize(1);
    markers_->primitives[0].type = markers_->primitives[0].BOX;
    markers_->primitives[0].dimensions.resize(3);
    markers_->primitives[0].dimensions[0] = wing->size().getX();
    markers_->primitives[0].dimensions[1] = wing->size().getY();
    markers_->primitives[0].dimensions[2] = wing->size().getZ();


    markers_->primitive_poses.resize(1);
    markers_->primitive_poses[0].position.x = world_origin.getX();
    markers_->primitive_poses[0].position.y = world_origin.getY();
    markers_->primitive_poses[0].position.z = world_origin.getZ() - wing->size().getZ()/2;
    markers_->primitive_poses[0].orientation.x = world_quat.getX();
    markers_->primitive_poses[0].orientation.y = world_quat.getY();
    markers_->primitive_poses[0].orientation.z = world_quat.getZ();
    markers_->primitive_poses[0].orientation.w = world_quat.getW();

    markers_->operation = markers_->ADD;
}