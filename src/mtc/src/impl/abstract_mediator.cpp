#include "impl/abstract_mediator.h"


std::vector<pcl::PointXYZ> Abstract_mediator::generate_Ground(const tf2::Vector3 origin, const float diameter, float resolution){
  std::vector<pcl::PointXYZ> ground_plane; 
  
  for (float x = origin.getX() - diameter * 1.5; x <= origin.getX() + diameter * 1.5; x += resolution){
    for (float y = origin.getY() - diameter * 1.5; y <= origin.getY() + diameter * 1.5; y += resolution){
      pcl::PointXYZ point(x,y, 0.4425f);
      ground_plane.push_back(point);     
    }
  }
  return ground_plane;
}

pcl::PointCloud< pcl::PointXYZ >::Ptr Abstract_mediator::vector_to_cloud(std::vector<pcl::PointXYZ>& vector){
  pcl::PointCloud< pcl::PointXYZ >::Ptr task_voxel(new pcl::PointCloud< pcl::PointXYZ >);
  for(pcl::PointXYZ& point : vector)
    task_voxel->push_back(point);
  
  return task_voxel;
}
