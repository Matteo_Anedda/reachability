#include "impl/robot.h"

void Robot::generate_access_fields(){
    for (int i = 0; i <= 2; i++){
        for (int j = 0; j <= 2; j++){
            if(i == 0 && j == 0) {continue;}
            if(i == 2 && j == 2) {continue;}
            if(i == 0) {

                access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0.8f*j,0))));
                access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,-0.8f*j,0))));
            } else if (j == 0){
                access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.8f*i,0,0))));
                access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.8f*i,0,0))));
            } else {
            access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.8f*i,0.8f*j,0))));
            access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.8f*i,0.8f*j,0))));
            access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0.8f*i,-0.8f*j,0))));
            access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-0.8f*i,-0.8f*j,0))));
            }

        };
    };
    access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(1.305f,0,0))));
    access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(-1.305f,0,0))));
    access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,1.305f,0))));
    access_fields_.push_back(new Field(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,-1.305f,0)))); 
    
}

float Robot::area_calculation(tf2::Transform& A, tf2::Transform& B, tf2::Transform& C){
    return std::abs(
                    (B.getOrigin().getX() * A.getOrigin().getY()) - (A.getOrigin().getX() * B.getOrigin().getY()) +
                    (C.getOrigin().getX() * B.getOrigin().getY()) - (B.getOrigin().getX() * C.getOrigin().getY()) +
                    (A.getOrigin().getX() * C.getOrigin().getY()) - (C.getOrigin().getX() * A.getOrigin().getY()))*0.5f;
}

void Robot::workload_checker(std::vector<int>& count_vector, tf2::Transform& obj){
    for (int i = 0; i < observers_.size(); i++){
        Abstract_robot_element_decorator *deco = dynamic_cast<Abstract_robot_element_decorator*>(observers_[i]);
        Wing* ceti = dynamic_cast<Wing*>(deco->wing());
        tf2::Transform A = ceti->world_tf() * ceti->bounds()[0];
        tf2::Transform B = ceti->world_tf() * ceti->bounds()[1];
        tf2::Transform C = ceti->world_tf() * ceti->bounds()[2];
        tf2::Transform D = ceti->world_tf() * ceti->bounds()[3];
            
        float full_area = area_calculation(A,B,C) + area_calculation(A,D,C);
        float sum = area_calculation(A,obj,D) + area_calculation(D,obj,C) + area_calculation(C,obj,B) + area_calculation(obj, B, A);

        if ((std::floor(sum*100)/100.f) <= full_area) { 
            count_vector[i]++; return;
        } else {
            continue;
        }
    }

    
    tf2::Transform A = tf_ * bounds_[0];
    tf2::Transform B = tf_ * bounds_[1];
    tf2::Transform C = tf_ * bounds_[2];
    tf2::Transform D = tf_ * bounds_[3];

    float full_area = area_calculation(A,B,C) + area_calculation(A,D,C);
    float sum = area_calculation(A,obj,D) + area_calculation(D,obj,C) + area_calculation(C,obj,B) + area_calculation(obj, B, A);
    if ((std::floor(sum*100)/100.f) <= full_area) count_vector.back()++; return;
}



bool Robot::check_single_object_collision(tf2::Transform& obj, std::string& str){

    tf2::Transform A = tf_ * root_tf_ * robot_root_bounds_[0];
    tf2::Transform B = tf_ * root_tf_ * robot_root_bounds_[1];
    tf2::Transform C = tf_ * root_tf_ * robot_root_bounds_[2];
    tf2::Transform D = tf_ * root_tf_ * robot_root_bounds_[3];

    float full_area = area_calculation(A,B,C) + area_calculation(A,D,C);
    float sum = area_calculation(A,obj,D) + area_calculation(D,obj,C) + area_calculation(C,obj,B) + area_calculation(obj, B, A);
    if ((std::floor(sum*100)/100.f) <= full_area) {return false; } 
    
    std::stringstream ss;
    ss << "base_" << name_.back();
    A = tf_ * bounds_[0];
    B = tf_ * bounds_[1];
    C = tf_ * bounds_[2];
    D = tf_ * bounds_[3];

    full_area = area_calculation(A,B,C) + area_calculation(A,D,C);
    sum = area_calculation(A,obj,D) + area_calculation(D,obj,C) + area_calculation(C,obj,B) + area_calculation(obj, B, A);
    if ((std::floor(sum*100)/100.f) <= full_area){
        str = ss.str();
        return true;
    }

    if (!observers_.empty()){
        std::vector<Abstract_robot_element*>::const_iterator it = observers_.begin();            
        while (it != observers_.end()) {
            Abstract_robot_element_decorator *deco = dynamic_cast<Abstract_robot_element_decorator*>(*it);
            Wing* ceti = dynamic_cast<Wing*>(deco->wing());
            tf2::Transform A = ceti->world_tf() * ceti->bounds()[0];
            tf2::Transform B = ceti->world_tf() * ceti->bounds()[1];
            tf2::Transform C = ceti->world_tf() * ceti->bounds()[2];
            tf2::Transform D = ceti->world_tf() * ceti->bounds()[3];
            
            full_area = area_calculation(A,B,C) + area_calculation(A,D,C);
            sum = area_calculation(A,obj,D) + area_calculation(D,obj,C) + area_calculation(C,obj,B) + area_calculation(obj, B, A);
            if ((std::floor(sum*100)/100.f) <= full_area) {
                str = ceti->name();
                return true;
            } else {
                ++it;
            }
        }
    }
    return false;
}

void Robot::reset(){
    observers_.clear();
    access_fields_.clear();
    generate_access_fields();
    tf_ = tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(0,0, tf().getOrigin().getZ()));
}

void Robot::notify(){ 
    for(Abstract_robot_element* wing : observers_) wing->update(tf_); 
    for(Abstract_robot_element* field : access_fields_) field->update(tf_);
}

bool Robot::check_robot_collision( Robot* rob){
    tf2::Transform A = tf_ * bounds_[0];
    tf2::Transform B = tf_ * bounds_[1];
    tf2::Transform C = tf_ * bounds_[2];
    tf2::Transform D = tf_ * bounds_[3];

    float full_area = area_calculation(A,B,C) + area_calculation(A,D,C);
    float sum = area_calculation(A,rob->tf(),D) + area_calculation(D,rob->tf(),C) + area_calculation(C,rob->tf(),B) + area_calculation(rob->tf(), B, A);
    if ((std::floor(sum*100)/100.f) <= full_area) return true;

    for(Abstract_robot_element* are : rob->observers()) {
        Wing_rviz_decorator* wrd = dynamic_cast<Wing_rviz_decorator*>(are);
        Wing* w = dynamic_cast<Wing*>(wrd->wing());

        tf2::Transform WA = w->world_tf() * w->bounds()[0];
        tf2::Transform WB = w->world_tf() * w->bounds()[1];
        tf2::Transform WC = w->world_tf() * w->bounds()[2];
        tf2::Transform WD = w->world_tf() * w->bounds()[3];
        sum = area_calculation(A,WA,D) + area_calculation(D,WA,C) + area_calculation(C,WA,B) + area_calculation(WA, B, A); if ((std::floor(sum*100)/100.f) <= full_area) return true;
        sum = area_calculation(A,WB,D) + area_calculation(D,WB,C) + area_calculation(C,WB,B) + area_calculation(WB, B, A); if ((std::floor(sum*100)/100.f) <= full_area) return true;
        sum = area_calculation(A,WC,D) + area_calculation(D,WC,C) + area_calculation(C,WC,B) + area_calculation(WC, B, A); if ((std::floor(sum*100)/100.f) <= full_area) return true;
        sum = area_calculation(A,WD,D) + area_calculation(D,WD,C) + area_calculation(C,WD,B) + area_calculation(WD, B, A); if ((std::floor(sum*100)/100.f) <= full_area) return true;
    }

    for (Abstract_robot_element* ar : observers_){
        Wing_rviz_decorator* wrd = dynamic_cast<Wing_rviz_decorator*>(ar);
        Wing* w = dynamic_cast<Wing*>(wrd->wing());

        tf2::Transform WA = w->world_tf() * w->bounds()[0];
        tf2::Transform WB = w->world_tf() * w->bounds()[1];
        tf2::Transform WC = w->world_tf() * w->bounds()[2];
        tf2::Transform WD = w->world_tf() * w->bounds()[3];

        full_area = area_calculation(WA,WB,WC) + area_calculation(WA,WD,WC);
        for(Abstract_robot_element* are : rob->observers()) {
            Wing_rviz_decorator* wrd = dynamic_cast<Wing_rviz_decorator*>(are);
            Wing* w = dynamic_cast<Wing*>(wrd->wing());

            tf2::Transform WA2 = w->world_tf() * w->bounds()[0];
            tf2::Transform WB2 = w->world_tf() * w->bounds()[1];
            tf2::Transform WC2 = w->world_tf() * w->bounds()[2];
            tf2::Transform WD2 = w->world_tf() * w->bounds()[3];
            sum = area_calculation(WA,WA2,WD) + area_calculation(WD,WA2,WC) + area_calculation(WC,WA2,WB) + area_calculation(WA2, WB, WA); if ((std::floor(sum*100)/100.f) <= full_area) return true;
            sum = area_calculation(WA,WB2,WD) + area_calculation(WD,WB2,WC) + area_calculation(WC,WB2,WB) + area_calculation(WB2, WB, WA); if ((std::floor(sum*100)/100.f) <= full_area) return true;
            sum = area_calculation(WA,WC2,WD) + area_calculation(WD,WC2,WC) + area_calculation(WC,WC2,WB) + area_calculation(WC2, WB, WA); if ((std::floor(sum*100)/100.f) <= full_area) return true;
            sum = area_calculation(WA,WD2,WD) + area_calculation(WD,WD2,WC) + area_calculation(WC,WD2,WB) + area_calculation(WD2, WB, WA); if ((std::floor(sum*100)/100.f) <= full_area) return true;
        }
    }
}



void Robot::register_observers(Abstract_robot_element* wd){
    Abstract_robot_element_decorator* decorator = dynamic_cast<Abstract_robot_element_decorator*>(wd);
    observers_.push_back(wd);
    std::vector<tf2::Transform> plane;
    bool found = false;
    int index = 0;

    if (decorator->wing()->relative_tf().getOrigin().getY()>0){
        for(long unsigned int i = 0; i < access_fields_.size(); i++){
            if ((access_fields_[i]->relative_tf().getOrigin().getY() > 0) && (access_fields_[i]->relative_tf().getOrigin().getX() == 0)){
                found = true;
                index = i;
                break;
            }
        }
        if(found) {access_fields_.erase(access_fields_.begin() + index); return;}
    } else if (decorator->wing()->relative_tf().getOrigin().getY()<0) {
        for(long unsigned int i = 0; i < access_fields_.size(); i++){
            if ((access_fields_[i]->relative_tf().getOrigin().getY() < 0) && (access_fields_[i]->relative_tf().getOrigin().getX() == 0)){
                found = true;
                index = i;
                break;
            }
        }
        if(found) {access_fields_.erase(access_fields_.begin() + index); return;}
    } else {
        for(long unsigned int i = 0; i < access_fields_.size(); i++){
            if (access_fields_[i]->relative_tf().getOrigin().getX() > 0){
                found = true;
                index = i;
                break;
            }
        }
        if(found) {access_fields_.erase(access_fields_.begin() + index); return;}
    }
};
