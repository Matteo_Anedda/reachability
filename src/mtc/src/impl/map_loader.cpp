#include "impl/map_loader.h"
#include <sstream>


Map_loader::Map_loader(XmlRpc::XmlRpcValue& map, XmlRpc::XmlRpcValue& task){
    std::regex reg("\\s+");
    std::vector<tf2::Transform> _map;

    ROS_INFO("load [map] data...");
    for (int i = 0; i < map.size();i++){
        std::string str = map[i];
        std::sregex_token_iterator to_it(str.begin(), str.end(),reg,-1);
        std::vector<std::string> temp{to_it, {}};
        tf2::Vector3 t(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2]));
        tf2::Quaternion so(std::stof(temp[3]), std::stof(temp[4]), std::stof(temp[5]), std::stof(temp[6]));
        
        _map.push_back(tf2::Transform(so.normalize(),t));
    }
    this->set_map(_map);
    ROS_INFO("[map] saved with %li entries...", _map.size());

    ROS_INFO("saving [target] positions...");
    std::vector<std::vector<tf2::Transform>> task_;
    for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it = task.begin(); it != task.end(); ++it){
        std::vector<tf2::Transform> task_per_robot;
        for (int i = 0; i < it->second.size(); i++){
            std::string str = it->second[i];
            std::sregex_token_iterator to_it(str.begin(), str.end(),reg,-1);
            std::vector<std::string> temp{to_it, {}};
            task_per_robot.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2]))));
        } 
        task_.push_back(task_per_robot);
    }
    this->set_task_grasps(task_);
    
    for (long unsigned int i = 0; i < task_.size(); i++) ROS_INFO("[target] for Robot %li saved with %li entries...", i, task_[i].size());
}

std::vector<std::vector<pcl::PointXYZ>> Map_loader::base_calculation(){

    ROS_INFO("calculating target orientation basic set...");
    std::vector<tf2::Quaternion> basic_rot;
    tf2::Quaternion x_rot(0,0,0,1);
    tf2::Quaternion y_rot(0,0,0.707,0.707);
    tf2::Quaternion z_rot(0,-0.707,0,0.707);
    basic_rot.push_back(x_rot.normalize());
    basic_rot.push_back(x_rot.inverse().normalize());
    basic_rot.push_back(y_rot.normalize());
    basic_rot.push_back(y_rot.inverse().normalize());
    basic_rot.push_back(z_rot.inverse().normalize());
    
    std::vector<std::vector<std::vector<tf2::Quaternion>>> target_orientation_grasps;
    for (long unsigned int i = 0; i < task_grasps_.size(); i++) {
        std::vector<std::vector<tf2::Quaternion>> quat;
        for (long unsigned int j = 0; j < task_grasps_[i].size(); j++) {
            quat.push_back(basic_rot);
        }
        target_orientation_grasps.push_back(quat);
    }
    target_rot_= target_orientation_grasps;

    ROS_INFO("basic set registered...");
    strategy_->inv_map_creation(this);

    ROS_INFO("init voxel...");
    std::vector<pcl::PointXYZ> voxelization = this->create_pcl_box();
    std::vector<std::vector<std::vector<int>>> base_target_map;
    base_target_map.resize(task_grasps_.size());
    for(long unsigned int i = 0; i < base_target_map.size();i++) base_target_map[i].resize(voxelization.size());

    ROS_INFO("forming base clouds...");
    strategy_->cloud_calculation(this);

    // OpenMP 
    ROS_INFO("start cloud quantization...");
    for(long unsigned int i = 0; i < target_cloud_.size();i++){
        for(long unsigned int j = 0; j < target_cloud_[i].size();j++){
            pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(0.2f);
            octree.setInputCloud(target_cloud_[i][j]);
            octree.addPointsFromInputCloud();
            double min_x, min_y, min_z, max_x, max_y, max_z;
            octree.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
            for(long unsigned int k = 0; k < voxelization.size(); k++) {
                pcl::PointXYZ p = voxelization[k];
                bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);
                if(isInBox && octree.isVoxelOccupiedAtPoint(p)) {
                    std::vector< int > pointIdxVec;
                    if(octree.voxelSearch(p, pointIdxVec)) if(!pointIdxVec.empty()) base_target_map[i][k].push_back(j);
                }

            }
        }
    }




    std::vector<std::vector<pcl::PointXYZ>> resulting;
    for(long unsigned int i = 0; i < base_target_map.size(); i++) {
        std::vector<pcl::PointXYZ> points_per_robot;
        for(int j = 0; j < base_target_map[i].size(); j++){
            if (base_target_map[i][j].size() == task_grasps_[i].size()) {
                points_per_robot.push_back(voxelization[j]);
            }
        }
        if (!points_per_robot.empty()) resulting.push_back(points_per_robot);
    }

    for (long unsigned int i = 0; i < resulting.size(); i++) {
        ROS_INFO("Robot %li got %li base positions to ckeck", i, resulting[i].size());
    }
    return resulting;
}


std::vector<pcl::PointXYZ> Abstract_map_loader::create_pcl_box(){
    tf2::Vector3 origin(0,0,0);
    float resolution = 0.4f;
    float diameter = 3.0f;
    unsigned char depth = 16;
    std::vector<pcl::PointXYZ> box; 
    octomap::OcTree* tree = new octomap::OcTree(resolution/2);
    for (float x = origin.getX() - diameter * 5 ; x <= origin.getX() + diameter * 5 ; x += resolution){
        for (float y = origin.getY() - diameter * 5 ; y <= origin.getY() + diameter * 5 ; y += resolution){
            for (float z = origin.getZ() - diameter * 1.5 ; z <= origin.getZ() + diameter * 1.5 ; z += resolution){
                octomap::point3d point(x,y,z);
                tree->updateNode(point, true);
            }
        }
    }

    for (octomap::OcTree::leaf_iterator it = tree->begin_leafs(depth), end = tree->end_leafs(); it != end; ++it){
        pcl::PointXYZ searchPoint(it.getCoordinate().x(), it.getCoordinate().y(), it.getCoordinate().z());
        box.push_back(searchPoint);
    }

    return box;
}

void Map_loader::write_task(Abstract_robot* robot){

    std::ofstream o(ros::package::getPath("mtc") + "/mtc_task_file/dummy.yaml");
    tf2::Transform target_start = task_grasps_[0].front();
    tf2::Transform target_end = task_grasps_[0].back();
    float x, y, z;
    x = target_end.getOrigin().getX();
    y = target_end.getOrigin().getY();
    z = target_end.getOrigin().getZ();

    std::stringstream ss;
    ss << "hand_" << robot->name().back();
    std::string hand = ss.str();
    std::stringstream sss;
    sss << "panda_" << robot->name().back() << "_link8";
    std::string last_link = sss.str();



    YAML::Node node;
    //YAML::Comment("yaml-language-server: $schema=/home/matteo/reachability/src/yaml_to_mtc/config/yaml_to_mtc_schema.json");
    YAML::Node planner_node;
    planner_node["id"] = "cartesian";
    planner_node["type"] = "CartesianPath";
    node["planners"].push_back(planner_node);
    planner_node.reset();

    planner_node["id"] = "sampling";
    planner_node["type"] = "PipelinePlanner";
    planner_node["properties"]["step_size"] = 0.005f;
    planner_node["properties"]["goal_joint_tolerance"] = static_cast<double>(0.00001f);
    node["planners"].push_back(planner_node);
    planner_node.reset();

    planner_node["id"] = "interpolation";
    planner_node["type"] = "JointInterpolationPlanner";
    node["planners"].push_back(planner_node);
    planner_node.reset();


    node["task"]["name"] = "Pick and Place test";
    node["task"]["properties"]["group"] = robot->name();
    node["task"]["properties"]["eef"] = hand;
    node["task"]["properties"]["hand_grasping_frame"] = last_link;
    node["task"]["properties"]["ik_frame"] = last_link;
    node["task"]["properties"]["hand"] = hand;

    YAML::Node stage;
    stage["name"] = "current";
    stage["type"] = "CurrentState";
    node["task"]["stages"].push_back(stage);
    stage.reset();

    stage["name"] = "move to ready";
    stage["type"] = "MoveTo";   
    stage["id"] = "ready";   
    stage["planner"] = "sampling";
    stage["propertiesConfigureInitFrom"]["source"] = "PARENT";
    stage["propertiesConfigureInitFrom"]["values"].push_back("group");
    stage["set"]["goal"] = "ready";
    node["task"]["stages"].push_back(stage);
    stage.reset();

    stage["type"] = "MoveTo";   
    stage["planner"] = "sampling";  
    stage["id"] = "hand_open";  
    stage["properties"]["group"] = hand; 
    stage["set"]["goal"] = "open";  
    node["task"]["stages"].push_back(stage);
    stage.reset();

    stage["type"] = "Connect";   
    stage["group_planner_vector"][robot->name()] = "sampling"; 
    stage["propertiesConfigureInitFrom"]["source"] = "PARENT"; 
    node["task"]["stages"].push_back(stage);
    stage.reset();

    stage["type"] = "SerialContainer";
    stage["name"] = "grasp";
    stage["properties_exposeTo"]["source"] = "task";
    stage["properties_exposeTo"]["values"] = YAML::Load("[eef, hand, group, ik_frame]");
    stage["propertiesConfigureInitFrom"]["source"] = "PARENT";
    stage["propertiesConfigureInitFrom"]["values"] = YAML::Load("[eef, hand, group, ik_frame]");
    //node["task"]["stages"].push_back(stage);

    YAML::Node stage_in_stage;
    stage_in_stage["type"] = "MoveRelative";
    stage_in_stage["planner"] = "cartesian";
    stage_in_stage["properties"]["link"] = last_link;
    stage_in_stage["properties"]["min_distance"] = 0.07f;
    stage_in_stage["properties"]["max_distance"] = 0.2f;
    stage_in_stage["propertiesConfigureInitFrom"]["source"] = "PARENT";
    stage_in_stage["propertiesConfigureInitFrom"]["values"] = YAML::Load("[group]");
    stage_in_stage["set"]["direction"]["vector"] = YAML::Load("{x: 0.0, y: 0.0, z: 1.0, header: { frame_id: " + last_link +" }}");
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset();

    stage_in_stage["type"] = "ComputeIK";
    stage_in_stage["properties"] = YAML::Load("{max_ik_solutions: 5}");
    stage_in_stage["set"]["ik_frame"]["isometry"]["translation"] = YAML::Load("{ x: 0.1, y: 0.0, z: 0.0 }");
    stage_in_stage["set"]["ik_frame"]["isometry"]["quaternion"] = YAML::Load("{ r: 1.571, p: 0.785, y: 1.571 }");
    stage_in_stage["set"]["ik_frame"]["link"] = last_link;

    YAML::Node properties_in_stage_in_stage;
    properties_in_stage_in_stage["source"]= "PARENT";
    properties_in_stage_in_stage["values"]= YAML::Load("[eef, group]");
    stage_in_stage["propertiesConfigureInitFrom"].push_back(properties_in_stage_in_stage);
    properties_in_stage_in_stage.reset();

    properties_in_stage_in_stage["source"]= "INTERFACE";
    properties_in_stage_in_stage["values"]= YAML::Load("[target_pose]");
    stage_in_stage["propertiesConfigureInitFrom"].push_back(properties_in_stage_in_stage);
    properties_in_stage_in_stage.reset();

    stage_in_stage["stage"]["type"] = "GenerateGraspPose";
    stage_in_stage["stage"]["propertiesConfigureInitFrom"]["source"] = "PARENT";
    stage_in_stage["stage"]["properties"]["object"] = "bottle";
    stage_in_stage["stage"]["properties"]["angle_delta"] = 1.571f;
    stage_in_stage["stage"]["properties"]["pregrasp"] = "open";
    stage_in_stage["stage"]["set"]["monitored_stage"] = "ready";
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset();

    stage_in_stage["type"] = "ModifyPlanningScene";
    stage_in_stage["set"]["allow_collisions"]["first"] = "bottle";
    stage_in_stage["set"]["allow_collisions"]["second"]["joint_model_group_name"] = hand;
    stage_in_stage["set"]["allow_collisions"]["allow"] = true;
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset();

    stage_in_stage["type"] = "MoveTo";
    stage_in_stage["planner"] = "sampling";
    stage_in_stage["properties"]["group"] = hand;
    stage_in_stage["set"]["goal"] = "close";
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset();   

    stage_in_stage["type"] = "ModifyPlanningScene";
    stage_in_stage["set"]["attach_object"]["object"] = "bottle";
    stage_in_stage["set"]["attach_object"]["link"] = last_link;
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset();  

    stage_in_stage["type"] = "MoveRelative";
    stage_in_stage["planner"] = "cartesian";
    stage_in_stage["id"] = "pick_up";
    stage_in_stage["propertiesConfigureInitFrom"]["source"] = "PARENT";
    stage_in_stage["propertiesConfigureInitFrom"]["values"] = YAML::Load("[group]");
    stage_in_stage["properties"]["min_distance"] = 0.1f;
    stage_in_stage["properties"]["max_distance"] = 0.2f;
    stage_in_stage["set"]["ik_frame"]["link"] = last_link;
    stage_in_stage["set"]["direction"]["vector"] = YAML::Load("{ x: 0.0, y: 0.0, z: 1.0 }");
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset();  
    node["task"]["stages"].push_back(stage);

    stage.reset();

    stage["type"] = "Connect";
    stage["group_planner_vector"][robot->name()] = "sampling";
    stage["propertiesConfigureInitFrom"]["source"] = "PARENT";
    node["task"]["stages"].push_back(stage);
    stage.reset();

    // satrtet here 
    stage["type"] = "SerialContainer";
    stage["name"] = "place";
    stage["properties_exposeTo"]["source"] = "task";
    stage["properties_exposeTo"]["values"] = YAML::Load("[eef, hand, group, ik_frame]");
    stage["propertiesConfigureInitFrom"]["source"] = "PARENT";

    stage_in_stage["type"] = "MoveRelative";
    stage_in_stage["planner"] = "cartesian";
    stage_in_stage["properties"]["link"] = last_link;
    stage_in_stage["properties"]["min_distance"] = 0.1f;
    stage_in_stage["properties"]["max_distance"] = 0.2f;
    stage_in_stage["propertiesConfigureInitFrom"]["source"] = "PARENT";
    stage_in_stage["propertiesConfigureInitFrom"]["values"] = YAML::Load("[group]");
    stage_in_stage["set"]["direction"]["vector"] = YAML::Load("{ x: 0.0, y: 0.0, z: -1.0}");
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset(); 

    stage_in_stage["type"] = "ComputeIK";
    stage_in_stage["properties"] = YAML::Load("{ max_ik_solutions: 5 }");
    stage_in_stage["set"]["ik_frame"]["isometry"]["translation"] = YAML::Load("{ x: 0.1, y: 0.0, z: 0.0 }");
    stage_in_stage["set"]["ik_frame"]["isometry"]["quaternion"] = YAML::Load("{ r: 1.571, p: 0.785, y: 1.571 }");
    stage_in_stage["set"]["ik_frame"]["link"] = last_link;

    properties_in_stage_in_stage["source"]= "PARENT";
    properties_in_stage_in_stage["values"]= YAML::Load("[eef, group]");
    stage_in_stage["propertiesConfigureInitFrom"].push_back(properties_in_stage_in_stage);
    properties_in_stage_in_stage.reset();

    properties_in_stage_in_stage["source"]= "INTERFACE";
    properties_in_stage_in_stage["values"]= YAML::Load("[target_pose]");
    stage_in_stage["propertiesConfigureInitFrom"].push_back(properties_in_stage_in_stage);
    properties_in_stage_in_stage.reset();

    stage_in_stage["stage"]["type"] = "GeneratePose";
    stage_in_stage["stage"]["set"]["monitored_stage"] = "pick_up";
    stage_in_stage["stage"]["set"]["pose"]["point"] = YAML::Load("{ x: " + std::to_string(x) + ", y: " + std::to_string(y) + ", z: 0.9305 }"); // Hier objekt
    stage_in_stage["stage"]["set"]["pose"]["orientation"] = YAML::Load("{ x: 0.0, y: 0.0, z: 0.0, w: 1.0}");
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset(); 

    stage_in_stage["type"] = "MoveTo";
    stage_in_stage["planner"] = "sampling";
    stage_in_stage["properties"]["group"] = hand;
    stage_in_stage["set"]["goal"] = "open";
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset(); 

    stage_in_stage["type"] = "ModifyPlanningScene";
    stage_in_stage["set"]["detach_object"]["object"]= "bottle";
    stage_in_stage["set"]["detach_object"]["link"]= last_link;
    stage_in_stage["set"]["allow_collisions"]["first"]= "bottle";
    stage_in_stage["set"]["allow_collisions"]["second"]["joint_model_group_name"]= hand;
    stage_in_stage["set"]["allow_collisions"]["allow"] = false;
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset(); 

    stage_in_stage["type"] = "MoveRelative";
    stage_in_stage["planner"] = "cartesian";
    stage_in_stage["properties"]["link"] = last_link;
    stage_in_stage["properties"]["min_distance"] = 0.07f;
    stage_in_stage["properties"]["max_distance"] = 0.2f;
    stage_in_stage["propertiesConfigureInitFrom"]["source"] = "PARENT";
    stage_in_stage["propertiesConfigureInitFrom"]["values"] = YAML::Load("[group]");
    stage_in_stage["set"]["direction"]["vector"] = YAML::Load("{x: 0.0, y: 0.0, z: -1.0, header:  { frame_id: " + last_link+ " }}");
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset(); 

    stage_in_stage["type"] = "MoveTo";
    stage_in_stage["planner"] = "sampling";
    stage_in_stage["properties"]["group"] = hand;
    stage_in_stage["set"]["goal"] = "close";
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset();    

    stage_in_stage["name"] = "move to ready";
    stage_in_stage["type"] = "MoveTo";
    stage_in_stage["planner"] = "sampling";
    stage_in_stage["propertiesConfigureInitFrom"]["source"]= "PARENT";
    stage_in_stage["propertiesConfigureInitFrom"]["values"].push_back("group");
    stage_in_stage["set"]["goal"] = "ready";
    stage["stages"].push_back(stage_in_stage);
    stage_in_stage.reset();    
    node["task"]["stages"].push_back(stage);
    stage.reset();  

    node["max_planning_solutions"] = 10;
    o << node;
    o.close();

}

