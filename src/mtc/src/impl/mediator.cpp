#include "impl/mediator.h"

#include "impl/wing_rviz_decorator.h"
#include <tf2/LinearMath/Scalar.h>

void Mediator::setup_rviz(){
    visualization_msgs::MarkerArray ma;
    for (int i = 0; i < wings_.size(); i++){
        Robot* r = dynamic_cast<Robot*>(robots_[i]);
        for (int j = 0; j < wings_[i].size(); j++){
            Wing_rviz_decorator* wrd = dynamic_cast<Wing_rviz_decorator*>(wings_[i][j]);
            Wing* w = dynamic_cast<Wing*>(wrd->wing());
            visualization_msgs::Marker marker;
            marker.header.frame_id = "map";
            marker.header.stamp = ros::Time();
            marker.ns = w->name();
            marker.id = 1;
            marker.type = visualization_msgs::Marker::CUBE;
            marker.action = visualization_msgs::Marker::ADD;
            marker.pose.position.x = w->world_tf().getOrigin().getX();
            marker.pose.position.y = w->world_tf().getOrigin().getY();
            marker.pose.position.z = w->world_tf().getOrigin().getZ();
            marker.pose.orientation.x = w->world_tf().getRotation().getX();
            marker.pose.orientation.y = w->world_tf().getRotation().getY();
            marker.pose.orientation.z = w->world_tf().getRotation().getZ();
            marker.pose.orientation.w = w->world_tf().getRotation().getW();
            marker.scale.x = w->size().getX();
            marker.scale.y = w->size().getY();
            marker.scale.z = w->size().getZ();
            marker.color.r = 1.0;
            marker.color.g = 1.0;
            marker.color.b = 1.0;
            marker.color.a = 1.0;
            ma.markers.push_back(marker);
        }
    }
    //pub_->publish(ma);
}

void Mediator::set_wings(std::vector<std::pair<std::vector<object_data>, int>>& wbp){

    for (int i =0; i < wbp.size(); i++){
        std::vector<Abstract_robot_element*> v;
        for (int j =0; j < wbp[i].first.size(); j++){
            Abstract_robot_element* are = new Wing_rviz_decorator( new Wing(wbp[i].first[j].name_, wbp[i].first[j].pose_,wbp[i].first[j].size_));
            v.push_back(are);
        }
        wings_.push_back(v);
    }

}

bool Mediator::check_collision( const int& robot){
    bool succ = true;
    std::vector<int> count_v;
    Robot* r = dynamic_cast<Robot*>(robots_[robot]);
    count_v.resize(r->observers().size()+1);
    std::string str;

    visualization_msgs::MarkerArray ma;
    for(long unsigned int j = 0; j < objects_[robot].size(); j++){
        visualization_msgs::Marker marker;
        marker.header.frame_id = "map";
        marker.header.stamp = ros::Time();
        marker.ns = "objects ";
        marker.id = j;
        marker.type = visualization_msgs::Marker::CUBE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = objects_[robot][j].getOrigin().getX();
        marker.pose.position.y = objects_[robot][j].getOrigin().getY();
        marker.pose.position.z = objects_[robot][j].getOrigin().getZ();
        marker.pose.orientation.x = objects_[robot][j].getRotation().getX();
        marker.pose.orientation.y = objects_[robot][j].getRotation().getY();
        marker.pose.orientation.z = objects_[robot][j].getRotation().getZ();
        marker.pose.orientation.w = objects_[robot][j].getRotation().getW();
        marker.scale.x = box_size.getX();
        marker.scale.y = box_size.getY();
        marker.scale.z = box_size.getZ();
        if(robots_[robot]->check_single_object_collision(objects_[robot][j], str)){
            marker.color.r = 0;
            marker.color.g = 1.0;
            marker.color.b = 0;
            marker.color.a = 1;
        } else {
            marker.color.r = 1;
            marker.color.g = 0;
            marker.color.b = 0;
            marker.color.a = 1.0;
            succ = false;
        }
        ma.markers.push_back(marker);
        robots_[robot]->workload_checker(count_v, objects_[robot][j]);
    }
    pub_->publish(ma);

    for (int& i : count_v){
        if(i == 0) {succ = false; break;}
    }
    return succ;
}


void Mediator::mediate(){
    ROS_INFO("assigne result to first robot...");
    std::vector<pcl::PointXYZ> grCenter = Abstract_mediator::generate_Ground(tf2::Vector3(0,0,0), 3.0f, 0.1f);
    std::vector<std::vector<tf2::Transform>> filter_per_robot;

    pcl::octree::OctreePointCloudSearch< pcl::PointXYZ >first_cloud(0.4f);
    first_cloud.setInputCloud(Abstract_mediator::vector_to_cloud(result_vector_[0]));
    first_cloud.addPointsFromInputCloud();
    std::vector<tf2::Transform> ground_per_robot;
    for(pcl::PointXYZ& p : grCenter){
        double min_x, min_y, min_z, max_x, max_y, max_z;
        first_cloud.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
        bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);
        if (isInBox && first_cloud.isVoxelOccupiedAtPoint(p)) {
        std::vector< int > pointIdxVec;
        if (first_cloud.voxelSearch(p, pointIdxVec)) ground_per_robot.push_back(tf2::Transform(tf2::Quaternion(0,0,0,1), tf2::Vector3(p.x, p.y, p.z)));
        }
    }

    for(int i = 0; i < objects_.size(); i++){
        if (i+1 < objects_.size()){
            for (long unsigned int j = objects_[i].size()-1; j > 0; j--){
                if(objects_[i][j].getOrigin().distance(objects_[i+1].back().getOrigin()) == 0) objects_[i+1].pop_back();
            }
        }
    }

    calculate(ground_per_robot);

    //swap
    Abstract_robot* ar = robots_[1];
    robots_[1] = robots_[0];
    robots_[0] = ar;
    
    calculate(ground_per_robot);


}

void Mediator::calculate(std::vector<tf2::Transform>& ground_per_robot){
    visualization_msgs::MarkerArray ma;
    int r1 = 0; 
    Robot* ceti1 = dynamic_cast<Robot*>(robots_[0]);
    for(int j = 0; j <= 7; j++){
        std::bitset<3> wing_config(j);
        build_wings(wing_config, r1);

        visualization_msgs::Marker m;
        m.header.frame_id = "map";
        m.header.stamp = ros::Time();
        m.ns = ceti1->name();
        m.id = 1;
        m.type = visualization_msgs::Marker::CUBE;
        m.action = visualization_msgs::Marker::ADD;
        m.pose.position.x = ceti1->tf().getOrigin().getX();
        m.pose.position.y = ceti1->tf().getOrigin().getY();
        m.pose.position.z = ceti1->tf().getOrigin().getZ();
        m.pose.orientation.x = ceti1->tf().getRotation().getX();
        m.pose.orientation.y = ceti1->tf().getRotation().getY();
        m.pose.orientation.z = ceti1->tf().getRotation().getZ();
        m.pose.orientation.w = ceti1->tf().getRotation().getW();
        m.scale.x = ceti1->size().getX();
        m.scale.y = ceti1->size().getY();
        m.scale.z = ceti1->tf().getOrigin().getZ()*2;
        m.color.r = 1.0;
        m.color.g = 1.0;
        m.color.b = 1.0;
        m.color.a = 1.0;
        ma.markers.push_back(m);
        pub_->publish(ma);
        ma.markers.clear();

        ros::Duration timer(0.10);

        for (long unsigned int i = 0; i < ground_per_robot.size(); i++){
            ceti1->set_tf(ground_per_robot[i]);
            // vis. robot 1

            for ( float p = 0; p < 2*M_PI; p += 0.0872665f){
                ceti1->rotate(0.0872665f);
                ceti1->notify();

                m.action = visualization_msgs::Marker::MODIFY;
                m.pose.position.x = ceti1->tf().getOrigin().getX();
                m.pose.position.y = ceti1->tf().getOrigin().getY();
                m.pose.position.z = ceti1->tf().getOrigin().getZ();
                m.pose.orientation.x = ceti1->tf().getRotation().getX();
                m.pose.orientation.y = ceti1->tf().getRotation().getY();
                m.pose.orientation.z = ceti1->tf().getRotation().getZ();
                m.pose.orientation.w = ceti1->tf().getRotation().getW();
                ma.markers.push_back(m);

                for (Abstract_robot_element* are : ceti1->observers()){
                    Wing_rviz_decorator* wrd = dynamic_cast<Wing_rviz_decorator*>(are);
                    ma.markers.push_back(*wrd->markers());
                }

                pub_->publish(ma);
                ma.markers.clear();

                if (check_collision(r1)) {
                    approximation(ceti1);
                }   else {
                    continue;
                }
                
               //timer.sleep();
            }
        }
        ceti1->reset();
        m.action = visualization_msgs::Marker::DELETEALL;
        ma.markers.push_back(m);
        pub_->publish(ma);
        ma.markers.clear();
    }
}

void Mediator::approximation(Robot* robot){
    Robot* ceti1 = dynamic_cast<Robot*>(robots_[1]);
    int r1 = 1;

    visualization_msgs::MarkerArray ma;

    for (int i = 0; i <= 7; i++){
        std::bitset<3> wing_config(i);
        build_wings(wing_config, r1);

        visualization_msgs::Marker m;
        m.header.frame_id = "map";
        m.header.stamp = ros::Time();
        m.ns = ceti1->name();
        m.id = 1;
        m.type = visualization_msgs::Marker::CUBE;
        m.action = visualization_msgs::Marker::ADD;
        m.pose.position.x = ceti1->tf().getOrigin().getX();
        m.pose.position.y = ceti1->tf().getOrigin().getY();
        m.pose.position.z = ceti1->tf().getOrigin().getZ();
        m.pose.orientation.x = ceti1->tf().getRotation().getX();
        m.pose.orientation.y = ceti1->tf().getRotation().getY();
        m.pose.orientation.z = ceti1->tf().getRotation().getZ();
        m.pose.orientation.w = ceti1->tf().getRotation().getW();
        m.scale.x = ceti1->size().getX();
        m.scale.y = ceti1->size().getY();
        m.scale.z = ceti1->tf().getOrigin().getZ()*2;
        m.color.r = 1.0;
        m.color.g = 1.0;
        m.color.b = 1.0;
        m.color.a = 1.0;
        ma.markers.push_back(m);
        pub_->publish(ma);
        ma.markers.clear();

        for (Abstract_robot_element* fields : robot->access_fields()) {
            ceti1->set_tf(fields->world_tf());
            //ROS_INFO("field size %i", robot->access_fields().size());
            for ( float p = 0; p < 2*M_PI; p += M_PI/2){
                ceti1->rotate(M_PI/2);
                ceti1->notify();

                m.action = visualization_msgs::Marker::MODIFY;
                m.pose.position.x = ceti1->tf().getOrigin().getX();
                m.pose.position.y = ceti1->tf().getOrigin().getY();
                m.pose.position.z = ceti1->tf().getOrigin().getZ();
                m.pose.orientation.x = ceti1->tf().getRotation().getX();
                m.pose.orientation.y = ceti1->tf().getRotation().getY();
                m.pose.orientation.z = ceti1->tf().getRotation().getZ();
                m.pose.orientation.w = ceti1->tf().getRotation().getW();
                ma.markers.push_back(m);
                
                for (Abstract_robot_element* are : ceti1->observers()){
                    Wing_rviz_decorator* wrd = dynamic_cast<Wing_rviz_decorator*>(are);
                    ma.markers.push_back(*wrd->markers());
                }

                pub_->publish(ma);
                ma.markers.clear();
                if (robot->check_robot_collision(ceti1)) continue;

                if (check_collision(r1)) {
                    write_file(robot, ceti1);
                } else {
                    continue;
                }

            }
            
            //relative_ground.push_back(pcl::PointXYZ(fields->world_tf().getOrigin().getX(), fields->world_tf().getOrigin().getY(), fields->world_tf().getOrigin().getZ()));
        }
        ceti1->reset();
        m.action = visualization_msgs::Marker::DELETEALL;
        ma.markers.push_back(m);
        pub_->publish(ma);
        ma.markers.clear();
    }
}


void Mediator::write_file(Robot* A, Robot* B){
    std::ofstream o(ros::package::getPath("mtc") + "/results/" + dirname_ + "/" + std::to_string(static_cast<int>(ros::Time::now().toNSec())) + ".yaml");
    double r,p,y;
    tf2::Matrix3x3 m(A->tf().getRotation());
    m.getRPY(r,p,y);


    float size_x = A->size().getX();
    float size_y = A->size().getY();
    float size_z = A->size().getZ();

    float pos_x = A->tf().getOrigin().getX();
    float pos_y = A->tf().getOrigin().getY();
    float pos_z = A->tf().getOrigin().getZ() *2 ;
    float rot_x = A->tf().getRotation().getX();
    float rot_y = A->tf().getRotation().getY();
    float rot_z = A->tf().getRotation().getZ();
    float rot_w = A->tf().getRotation().getW();





    std::stringstream ss;
    ss << "{ 'objects' : [ \n";
    ss << "{ 'id' : 'table" << A->name().back() << "_wheel_1', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << A->name().back() << "_wheel_2', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }},\n";
    ss << "{ 'id' : 'table" << A->name().back() << "_wheel_3', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << A->name().back() << "_wheel_4', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << A->name().back() << "_body_front', 'pos': { 'x': 0,'y': 0, 'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << A->name().back() << "_body_back', 'pos': { 'x': 0,'y': 0,'z': 0.45 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << A->name().back() << "_body_left', 'pos': { 'x': 0,'y': 0,'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << A->name().back() << "_body_right', 'pos': { 'x': 0,'y': 0,'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << A->name().back() << "_table_top', 'pos': { 'x': " << std::to_string(pos_x) << " , 'y': "<< std::to_string(pos_y) << " , 'z': "<< std::to_string(pos_z) << " },'size': { 'length': "<< std::to_string(size_x) << " ,'width': "<< std::to_string(size_y) << " ,'height': "<< std::to_string(size_z) << " },'orientation': { 'x': " << std::to_string(rot_x) << " , 'y': " << std::to_string(rot_y) << " , 'z': " << std::to_string(rot_z) << " , 'w': " << std::to_string(rot_w) << " },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }, 'rpy': { 'r': " << std::to_string(r) << " , 'p': " << std::to_string(p) << " , 'y': " << std::to_string(y) << " } },\n";
    
    m.setRotation(B->tf().getRotation());
    m.getRPY(r,p,y);

    size_x = B->size().getX();
    size_y = B->size().getY();
    size_z = B->size().getZ();

    pos_x = B->tf().getOrigin().getX();
    pos_y = B->tf().getOrigin().getY();
    pos_z = B->tf().getOrigin().getZ() *2 ;
    rot_x = B->tf().getRotation().getX();
    rot_y = B->tf().getRotation().getY();
    rot_z = B->tf().getRotation().getZ();
    rot_w = B->tf().getRotation().getW();

    ss << "{ 'id' : 'table" << B->name().back() << "_wheel_1', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << B->name().back() << "_wheel_2', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }},\n";
    ss << "{ 'id' : 'table" << B->name().back() << "_wheel_3', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << B->name().back() << "_wheel_4', 'pos': { 'x': 0,'y': 0,'z': 0.06 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << B->name().back() << "_body_front', 'pos': { 'x': 0,'y': 0, 'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << B->name().back() << "_body_back', 'pos': { 'x': 0,'y': 0,'z': 0.45 },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << B->name().back() << "_body_left', 'pos': { 'x': 0,'y': 0,'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << B->name().back() << "_body_right', 'pos': { 'x': 0,'y': 0,'z': 0.50  },'size': { 'length': 0.12,'width': 0.12,'height': 0.12 },'orientation': { 'w': 1 },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }}, \n";
    ss << "{ 'id' : 'table" << B->name().back() << "_table_top', 'pos': { 'x': " << std::to_string(pos_x) << ", 'y': "<< std::to_string(pos_y) << ", 'z': "<< std::to_string(pos_z) << " },'size': { 'length': "<< std::to_string(size_x) << " ,'width': "<< std::to_string(size_y) << " ,'height': "<< std::to_string(size_z) << " },'orientation': { 'x': " << std::to_string(rot_x) << " , 'y': " << std::to_string(rot_y) << " , 'z': " << std::to_string(rot_z) << " , 'w': " << std::to_string(rot_w) << " },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 }, 'rpy': { 'r': " << std::to_string(r) << ", 'p': " << std::to_string(p) << ", 'y': " << std::to_string(y) << "}},\n";


    for (Abstract_robot_element* ae : A->observers()){
        Wing_rviz_decorator* rd = dynamic_cast<Wing_rviz_decorator*>(ae);
        float x = rd->wing()->world_tf().getOrigin().getX();
        float y = rd->wing()->world_tf().getOrigin().getY();
        float z = rd->wing()->world_tf().getOrigin().getZ();
        float qx = rd->wing()->world_tf().getRotation().getX();
        float qy = rd->wing()->world_tf().getRotation().getY();
        float qz = rd->wing()->world_tf().getRotation().getZ();
        float qw = rd->wing()->world_tf().getRotation().getW();
        Wing* w = dynamic_cast<Wing*>(rd->wing());

        float length = w->size().getX();
        float width = w->size().getY();
        float height = w->size().getZ();

        ss << "{ 'id': '" << w->name() << "' , 'pos': { 'x': "<< std::to_string(x) << " , 'y': "<< std::to_string(y) << " , 'z': "<< std::to_string(z - 0.25*height) << " } , 'size': { 'length': "<< std::to_string(length) << " , 'width': "<< std::to_string(width) << " , 'height': "<< std::to_string(height) << " } , 'orientation': { 'x': "<< std::to_string(qx) << " , 'y': "<< std::to_string(qy) << " , 'z': "<< std::to_string(qz) << " , 'w': "<< std::to_string(qw) << " } , 'color': { 'r': 0.15 , 'g': 0.15 , 'b': 0.15 } }, \n";
    }

    
    for (Abstract_robot_element* ae : B->observers()){
        Wing_rviz_decorator* rd = dynamic_cast<Wing_rviz_decorator*>(ae);
        float x = rd->wing()->world_tf().getOrigin().getX();
        float y = rd->wing()->world_tf().getOrigin().getY();
        float z = rd->wing()->world_tf().getOrigin().getZ();
        float qx = rd->wing()->world_tf().getRotation().getX();
        float qy = rd->wing()->world_tf().getRotation().getY();
        float qz = rd->wing()->world_tf().getRotation().getZ();
        float qw = rd->wing()->world_tf().getRotation().getW();
        Wing* w = dynamic_cast<Wing*>(rd->wing());

        float length = w->size().getX();
        float width = w->size().getY();
        float height = w->size().getZ();

        ss << "{ 'id': '" << w->name() << "',  'pos': { 'x': "<< std::to_string(x) << ", 'y': "<< std::to_string(y) << " , 'z': "<< std::to_string(z - 0.25*height) << " },'size': { 'length': "<< std::to_string(length) << ",'width': "<< std::to_string(width) << ",'height': "<< std::to_string(height) << " },'orientation': { 'x': "<< std::to_string(qx) << ", 'y': "<< std::to_string(qy) << ", 'z': "<< std::to_string(qz) << ", 'w': "<< std::to_string(qw) << " },'color': { 'r': 0.15,'g': 0.15,'b': 0.15 } }, \n";
    }
    tf2::Transform tf_arm =  A->tf() * A->root_tf();
    float arm_x = tf_arm.getOrigin().getX();
    float arm_y = tf_arm.getOrigin().getY();
    float arm_z = tf_arm.getOrigin().getZ();
    float arm_qx = tf_arm.getRotation().getX();
    float arm_qy = tf_arm.getRotation().getY();
    float arm_qz = tf_arm.getRotation().getZ();
    float arm_qw = tf_arm.getRotation().getW();

    ss << "{ 'id': 'arm" << A->name().back() << "','type': 'ARM','pos': { 'x': " << std::to_string(arm_x) << ", 'y': " << std::to_string(arm_y) << ", 'z': 0.89 },'size': { },'orientation': { 'x': " << std::to_string(arm_qx) <<", 'y': " << std::to_string(arm_qy) << ", 'z': " << std::to_string(arm_qz) << ", 'w': " << std::to_string(arm_qw) << " },'color': { 'r': 1.00,'g': 1.00,'b': 1.00 } }, \n";

    tf_arm =  B->tf() * B->root_tf();
    arm_x = tf_arm.getOrigin().getX();
    arm_y = tf_arm.getOrigin().getY();
    arm_z = tf_arm.getOrigin().getZ();
    arm_qx = tf_arm.getRotation().getX();
    arm_qy = tf_arm.getRotation().getY();
    arm_qz = tf_arm.getRotation().getZ();
    arm_qw = tf_arm.getRotation().getW();

    ss << "{ 'id': 'arm" << B->name().back() << "','type': 'ARM','pos': { 'x': " << std::to_string(arm_x) << ", 'y': " << std::to_string(arm_y) << ", 'z': 0.89 },'size': { },'orientation': { 'x': " << std::to_string(arm_qx) <<", 'y': " << std::to_string(arm_qy) << ", 'z': " << std::to_string(arm_qz) << ", 'w': " << std::to_string(arm_qw) << " },'color': { 'r': 1.00,'g': 1.00,'b': 1.00 } } \n";

    ss << "]}";

    o << ss.str();
    o.close();
    
}

void Mediator::build_wings(std::bitset<3>& wing, int& robot){
    std::bitset<3> result = robots_[robot]->observer_mask() & wing;
    Robot* ceti = dynamic_cast<Robot*>(robots_[robot]);
    
    for (std::size_t i = 0; i < result.size(); i++){
        if (result[i]){
            ceti->register_observers(wings_[robot][i]);
        }
    }

    visualization_msgs::MarkerArray ma;
    for (Abstract_robot_element* are : ceti->observers()){
        Wing_rviz_decorator* wad = dynamic_cast<Wing_rviz_decorator*>(are);
        wad->markers()->action = visualization_msgs::Marker::ADD;
        ma.markers.push_back(*wad->markers());
    }
    pub_->publish(ma);
}

void Mediator::publish(Robot* ar){
    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time();
    marker.ns = ar->name();
    marker.id = 1;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::MODIFY;
    marker.pose.position.x = ar->tf().getOrigin().getX();
    marker.pose.position.y = ar->tf().getOrigin().getY();
    marker.pose.position.z = ar->tf().getOrigin().getZ();
    marker.pose.orientation.x = ar->tf().getRotation().getX();
    marker.pose.orientation.y = ar->tf().getRotation().getY();
    marker.pose.orientation.z = ar->tf().getRotation().getZ();
    marker.pose.orientation.w = ar->tf().getRotation().getW();
    marker.scale.x = ar->size().getX();
    marker.scale.y = ar->size().getY();
    marker.scale.z = ar->size().getZ();
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    marker.color.a = 1.0;
    for (auto w : ar->observers()){
        Wing_rviz_decorator* wrd = dynamic_cast<Wing_rviz_decorator*>(w);
        ma.markers.push_back(*wrd->markers());
    }
    
    //pub_->publish(ma);
}
