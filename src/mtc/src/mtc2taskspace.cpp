#include "reader/abstract_param_reader.h"
#include "reader/task_space_reader.h"
#include "reader/robot_reader.h"
#include <regex>

int main(int argc, char **argv){
    ros::init(argc, argv, "mtc2taskspace");
    std::shared_ptr<ros::NodeHandle> n(new ros::NodeHandle);

    std::unique_ptr<Abstract_param_reader> ts_reader(new Task_space_reader(n));
    ts_reader->read();
    return 0;
}