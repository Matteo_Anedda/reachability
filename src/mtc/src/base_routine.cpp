#include "impl/abstract_map_loader.h"
#include "impl/abstract_strategy.h"
#include "impl/abstract_robot.h"
#include "impl/abstract_robot_element.h"
#include "impl/abstract_robot_element_decorator.h"
#include "impl/abstract_mediator.h"
#include "impl/mediator.h"
#include "impl/wing_rviz_decorator.h"
#include "impl/field_rviz_decorator.h"
#include "impl/wing.h"
#include "impl/field.h"
#include "impl/robot.h"
#include "impl/map_loader.h"
#include "impl/base_by_rotation.h"
#include <xmlrpcpp/XmlRpc.h>
#include <filesystem>
#include "reader/abstract_param_reader.h"
#include "reader/robot_reader.h"
#include "reader/wing_reader.h"





int main(int argc, char **argv){
    ros::init(argc, argv, "base_routine");
    std::shared_ptr<ros::NodeHandle> n(new ros::NodeHandle);

    XmlRpc::XmlRpcValue map, task, resources;

    n->getParam("/data",map);
    n->getParam("/objects",resources);
    n->getParam("/task/groups",task);

    std::string filename;
    n->getParam("/resource_name", filename);

    if(std::filesystem::exists(ros::package::getPath("mtc") + "/results/" +  filename)) std::filesystem::remove_all(ros::package::getPath("mtc") + "/results/" +  filename);
    std::filesystem::create_directory(ros::package::getPath("mtc") + "/results/" +  filename);
    
    Abstract_map_loader* map_loader = new Map_loader(map, task);
    Abstract_strategy* strategy = new Base_by_rotation();

    map_loader->set_strategy(strategy);
    std::vector<std::vector<pcl::PointXYZ>> results = map_loader->base_calculation();

    Abstract_mediator* mediator = new Mediator(map_loader->task_grasps(), new ros::Publisher(n->advertise< visualization_msgs::MarkerArray >("visualization_marker_array", 1)));
    mediator->set_result_vector(results);
    mediator->set_dirname(filename);


    std::unique_ptr<Abstract_param_reader> robot_reader(new Robot_reader(n));
    robot_reader->read();
    std::vector<object_data> rd = static_cast<Robot_reader*>(robot_reader.get())->robot_data();
    for (int i = 0; i < rd.size() ;i++) mediator->connect_robots(new Robot(rd[i].name_, rd[i].pose_, rd[i].size_));

    std::unique_ptr<Abstract_param_reader> wing_reader(new Wing_reader(n));
    wing_reader->read();
    auto wd = static_cast<Wing_reader*>(wing_reader.get())->wing_data();

    for (int i = 0; i < mediator->robots().size(); i++){
        for(object_data& w : wd[i].first){
            w.pose_ = mediator->robots()[i]->tf().inverse() * w.pose_;
        }
        mediator->robots()[i]->set_observer_mask(static_cast<wing_config>(wd[i].second));
    }

    mediator->set_wings(wd);
    mediator->mediate();



    
    //map_loader->write_task(robo);

    //free(rviz_right);
    //free(rviz_mid);
    //free(rviz_left);


    free(map_loader);
    free(strategy);

    //free(robo);

    //free(robo2);

    return 0;
}