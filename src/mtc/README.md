# MTC 
MTC is the core package developed with different design patterns to ensure both extensibility and variability.

## executables
MTC consists of 3 executables, with each computing .yaml files for the next subsequent module, according the following strcture.
> mtc2taskspace >> base_routine >> cell_routine

### mtc2taskspace 
From a bare Ceti-resource file representing an arbitrary scene with specific locations, located in the /resources directory. Mtc2taskspace computes a new file and associates each object with a drop-of-location to a robot. The shared space between robots is expressed by identical location entries for each robot involved. <br/>
mtc/mtc/mtc2taskspace.cpp (/resources/{resource}) -> (/descriptions/{resource}) <br/>
roslaunch mtc mtc2taskspace.launch resource='dummy.yaml'


### base_routine  
Calculates the workspace from a descriptions file (/descriptions/{description}) and a precomputed reachability map (/map/{map}), combined with information of a resource file (/resources/{resource}). Creates the directory (results/{resource}/ros::time.yaml}) with the resulting files as contents. <br/> 
mtc/src/base_routine.cpp (/map/{map}, /resources/{resource}, /descriptions/{descriptions}) -> (/results/{resource}/ros::time.yaml) <br/>
roslaunch mtc base_routine.launch resource='dummy.yaml' map='dummy.yaml' description='dummy.yaml'
> Replace already existing result folder if exists.
>> Resulting files can break the yaml schema, because each robot has an extra rpy element which is important for setting robot pose in franca_description. Also each 8th entry must be a table[0-9]+_table_top id. Cosmetic data (table_wheel...) included as placeholder

### cell_routine  
Designed to load a resource file (+ with additional rpy component in the right order) and execute a task with moveit task constructor. A task description can also be the /descriptions/{description} in the example. <br/>
mtc/src/cell_routine.cpp (/results/{result}, /descriptions/{task}) -> () <br/>
roslaunch mtc base_routine.launch result='/dummy/dummy.yaml' task='dummy.yaml' (is coop) <br/>
roslaunch mtc base_routine.launch result='/dummy/dummy.yaml' task='dummy2.yaml' (is parallel) 
> Christoph's parser could not be integrated because there is no way to have multiple robots compute a task.



## Current state
State: in progress. Every executable is working with some explained downfalls that should be fixed in the next days. Implementing or omitting ideas because some modules do not work as expected took a lot of time. The actual results (62 files for dummy.yaml) can be loaded and run. I tested 10/62 myself, with surprisingly good results. The following list shows open and closed ToDos.<br/>

:ballot_box_with_check: present a faster work-space execution <br/>
:ballot_box_with_check: generate more solutions for 1 dump file <br/>
:ballot_box_with_check: present solution for general robots <br/>
:ballot_box_with_check: elaborate the moveit task constructor <br/>
:ballot_box_with_check: present more general task files <br/>
:black_square_button: include cosmetic values <br/>
:ballot_box_with_check: passing yaml in xacro (franca_description) <br/>
:ballot_box_with_check: parallel tasks (Docker already downloaded, just have to try) (main focus) <br/>
:black_square_button: fix acm <br/>
:black_square_button: include ferdinants work (main focus)<br/>

Im working non stop, so don't forget to pull. <br/>
[Git Docker](https://github.com/cambel/moveit_simultaneous_motions_demo.git "github link for parallel docker")
