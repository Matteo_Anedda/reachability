#ifndef TASK_VIEWER_H
#define TASK_VIEWER_H

#include <ros/ros.h>
#include <ros/package.h>
#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/menu_handler.h>
#include <tf/transform_broadcaster.h>
#include <fstream>
#include "yaml-cpp/yaml.h"
#include <tf/tf.h>
#include <eigen_conversions/eigen_msg.h>
#include <math.h>

using namespace visualization_msgs;
using namespace interactive_markers;
boost::shared_ptr<interactive_markers::InteractiveMarkerServer> server;
std::vector<interactive_markers::MenuHandler::EntryHandle> all_entrys;
interactive_markers::MenuHandler menu_handler;

// Alle Funktionen hinsichtlich der Serverkommunkation und des erstellens von Interaktiven Marker
InteractiveMarkerControl& makeBoxControl( InteractiveMarker &msg, std_msgs::ColorRGBA& col);
void frameCallback(const ros::TimerEvent&);
void processFeedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback );
void make6DofMarker(visualization_msgs::InteractiveMarker& int_marker);

// Initiallisierung des Menus
void initMenu(std::vector<interactive_markers::MenuHandler::EntryHandle> &all_entrys);

// Folgende Optionen sind, abhängig der Operaktionsketten-Beschaffenheit, über Rechtsklick auf das 1. Glied (glied_0), möglich.  
/**
 * Fügt ein Kettenglied hinzu
 * 
 * @param feedback 
 */
void add_node( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback );
/**
 * Füge schneidet die Ketten in disjunkte Teilketten 
 * 
 * @param feedback 
 */
void split_here( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback );

/**
 * Dokemtation der Aufgabenbeschreibung
 * 
 * @param feedback 
 */
void write_yaml( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback );

/**
 * Transformation eines kooperativen Glieds 
 * 
 * @param feedback 
 */
void add_coop_node( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback );


#endif