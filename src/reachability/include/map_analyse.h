#ifndef MAP_ANALYSE_H
#define MAP_ANALYSE_H

#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/String.h"
#include "visualization_msgs/InteractiveMarker.h"
#include <tf/transform_broadcaster.h>
#include <regex>

#include "octomap/octomap.h"
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>

#include "geometry_msgs/Vector3.h"
#include "moveit/move_group_interface/move_group_interface.h"
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <tf2/LinearMath/Transform.h>
#include "math.h" 
#include <fstream>
#include "yaml-cpp/yaml.h"
#include <xmlrpcpp/XmlRpc.h>

// Teilung der OK in Teilketten mit Subketten für kooperative Operationen
void chaining(std::vector<std::vector<tf2::Vector3>>& ok, std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total);

// Orientierungen für jedes Kettenglied generieren

/**
 * Generiert alle Orienteirungen für eine Aufgabe
 * @param quat Quaternion Vektor für alle Orientierungen
 */
void task_sphere(std::vector<tf2::Quaternion>& quat );

/**
 * Generiert Box Struktur zum Abtasten des Octree
 * 
 * @param origin Mittelpunkt der Struktur
 * @param diameter Intervall
 * @param resolution Auflösung 
 * @return std::vector<pcl::PointXYZ> Input für Voxel_Search
 */
std::vector<pcl::PointXYZ> BoxTree(const tf2::Vector3& origin, float diameter, float resolution);

/**
 * XY-Ebene
 * @param origin Mittelpunkt
 * @param diameter Intervall
 * @param resolution Auflösung
 * @return std::vector<pcl::PointXYZ> Input für Voxel_Search
 */
std::vector<pcl::PointXYZ> generateGround(const tf2::Vector3& origin, float diameter, float resolution);

/**
 * Umwandlung in eine PointCloud 
 * 
 * @param vector Vektors mit pcl::Punkten 
 * @return pcl::PointCloud< pcl::PointXYZ >::Ptr  PointCloud
 */
pcl::PointCloud< pcl::PointXYZ >::Ptr vector_to_cloud(std::vector<pcl::PointXYZ>& vector);

// Filtert den Arbeitsraum nach Schnittmengen in den Subketten

/**
 * Filtert die BoxTree Struktur nach Kettengliedern bezüglich der Teilkette
 * 
 * @param intersections Resultat
 * @param ok_total Operationskette mit Subketten pro kooperativem Teil
 * @param voxel_dictionary Vektor aus Boxtree Struktur mit Kettenglied abhängigen Einträgen
 */
void intersection_filter(std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, float>>>>& intersections, std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total, 
  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>>>& voxel_dictionary);


// Marker pro Kettenlied, voxelisiert und auf den XY Ebene projeziert 

/**
 * Voxelisierte Marker pro Kettenglied
 * 
 * @param marker_arr 
 * @param ok_total 
 * @param voxel_dictionary 
 */
void marker_voxel(visualization_msgs::MarkerArray& marker_arr, std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total, 
  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>>>& voxel_dictionary);

/**
 * Marker pro Kettenglied 
 * 
 * @param marker_arr 
 * @param P_total 
 * @param index_total 
 */
void marker_per_glied(visualization_msgs::MarkerArray& marker_arr, std::vector<std::vector<std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>>& P_total,
  std::vector<std::vector<std::vector<float>>>& index_total);

/**
 * Marker auf der XY-Ebene 
 * 
 * @param marker_arr 
 * @param ok_total 
 * @param voxel_dictionary 
 * @param resolution 
 * @param ground 
 */
void marker_voxel_ground(visualization_msgs::MarkerArray& marker_arr, std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total, 
  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>>>& voxel_dictionary, float& resolution,
  std::vector<pcl::PointXYZ>& ground);


// Marker pro Schnittmenge, voxelisiert und auf den XY Ebene projeziert 

/**
 * Marker Pro Schnittmenge
 * 
 * @param marker_arr 
 * @param intersections 
 */
void marker_per_intersection(visualization_msgs::MarkerArray& marker_arr, std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, float>>>>& intersections);

/**
 * Marker pro Schnittmenge auf XY-Ebene
 * 
 * @param marker_arr 
 * @param ground 
 */
void marker_intersection_to_ground(visualization_msgs::MarkerArray& marker_arr, std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, float>>>>& ground);



#endif
