#ifndef REACH_MAP_H
#define REACH_MAP_H

#include "ros/ros.h"
#include <ros/package.h>

#include "visualization_msgs/InteractiveMarker.h"
#include "geometry_msgs/Vector3.h"
#include "math.h"
#include <moveit/robot_model_loader/robot_model_loader.h>
#include "moveit/move_group_interface/move_group_interface.h"
#include <tf/tf.h>
#include <eigen_conversions/eigen_msg.h>

#include <fstream>
#include <regex>
#include <thread>
#include "yaml-cpp/yaml.h"

// Initialisierung der Generation aller Posen
void generate_poses(std::vector<tf2::Vector3>& voxels, std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses , float& resolution);

// kubische Diskretisierung
void cube_dec(const float& abstand, const float& resolution, std::vector<tf2::Vector3>& voxels);

// sphärische Diskretisierung
void sphere_sample(const float& radius, const int& count, const tf2::Vector3& origin, std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses );
void sphere_Deserno(const float& radius, const int& count, const tf2::Vector3& origin, std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses );

// Spaltung der Posen in n Disjunkte Mengen, welche auf n Threads verteilt werden
std::vector<std::vector<std::pair<geometry_msgs::Pose, bool>>> split(std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses, size_t n);

// kinematische Berechnung pro Menge an Posen
void f_kin(std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses, robot_state::RobotStatePtr ks, const robot_state::JointModelGroup* jmg);


#endif

