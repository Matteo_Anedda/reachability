#include "../include/reach_map.h"
#define _USE_MATH_DEFINES
// for convenience

void cube_dec(const float& abstand, const float& resolution, std::vector<tf2::Vector3>& voxels){
  for (float x = 0 - abstand; x <= -0.22f + abstand; x+= resolution){
      for(float y = 0 - abstand; y <= 0 + abstand; y+= resolution){
          for(float z = 0.885f ; z <= 0.885f + abstand; z+= resolution){
              tf2::Vector3 p(x,y,z);
              voxels.push_back(p);
          }
      }
  }
}


void sphere_sample(const tf2::Vector3& origin, std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses ){
  geometry_msgs::Pose pose;
  const double DELTA = M_PI / 2.0f;
  for (double phi = 0; phi < 2*M_PI; phi += DELTA){
    for (double theta = 0; theta <= M_PI*0.5; theta += DELTA){
      tf2::Quaternion rot;
      rot.setRPY(0, ((M_PI / 2) + theta), phi);
      rot.normalize();
      pose.position.x = origin.getX();
      pose.position.y = origin.getY();
      pose.position.z = origin.getZ();
      pose.orientation.x = rot.getX();
      pose.orientation.y = rot.getY();
      pose.orientation.z = rot.getZ();
      pose.orientation.w = rot.getW();
      all_poses.push_back(std::pair<geometry_msgs::Pose, bool>(pose,false));
    }
  }
}


void sphere_Deserno(const float& radius, const int& count, const tf2::Vector3& origin, std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses ){
  geometry_msgs::Pose pose;

  float alpha, d, m_nu, d_nu, d_phi, nu, m_phi, phi;
  alpha = 4.0f * M_PI * 1 * 1 / count;
  d = sqrt(alpha);
  m_nu = round(M_PI / d);
  d_nu = M_PI / m_nu;
  d_phi = alpha / d_nu;
  for (int i = 0; i < m_nu; i++){
    nu = M_PI * (i + 0.5f) / m_nu;
    m_phi = round(2 * M_PI * sin(nu) / d_phi);
    for(int y = 0; y < m_phi; y++){
      if(radius * cos(nu) + origin.getZ() + 0.0001 >= origin.getZ()){
        tf2::Quaternion rot;
        phi = 2 * M_PI * y / m_phi;

        rot.setRPY(0, ((M_PI / 2) + nu), phi);
        rot.normalize();


        pose.position.x = origin.getX();
        pose.position.y = origin.getY();
        pose.position.z = origin.getZ();
        pose.orientation.x = rot.getX();
        pose.orientation.y = rot.getY();
        pose.orientation.z = rot.getZ();
        pose.orientation.w = rot.getW();

        all_poses.push_back(std::pair<geometry_msgs::Pose, bool>(pose,false));
      }
    }
  }
}


void generate_poses(std::vector<tf2::Vector3>& voxels, std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses , float& resolution){
  for(tf2::Vector3& voxel : voxels) 
    //sphere_sample(resolution, 10, voxel, all_poses);
    sphere_sample(voxel, all_poses);
}

std::vector<std::vector<std::pair<geometry_msgs::Pose, bool>>> split(std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses, size_t n){
  std::vector<std::vector<std::pair<geometry_msgs::Pose, bool>>> result;

  size_t length = all_poses.size() / n;
  size_t remain = all_poses.size() % n;

  size_t begin = 0;
  size_t end = 0;

  for (size_t i = 0; i < std::min(n, all_poses.size()); ++i){
    end += (remain > 0) ? (length + !!(remain--)) : length;
    result.push_back(std::vector<std::pair<geometry_msgs::Pose, bool>>(all_poses.begin() + begin, all_poses.begin() + end));
    begin = end;
  }
  return result;
}

void f_kin(std::vector<std::pair<geometry_msgs::Pose, bool>>& all_poses, robot_state::RobotStatePtr ks, const robot_state::JointModelGroup* jmg){
  for(std::vector<std::pair<geometry_msgs::Pose, bool>>::iterator it = all_poses.begin(); it != all_poses.end(); ++it){
    Eigen::Isometry3d pose;
    tf::poseMsgToEigen(it->first,pose);
    bool success = ks->setFromIK(jmg, pose,5.0);
    ROS_INFO("test");
    if(success)
      ROS_INFO("richtig");
      it->second = true;
        
  }
}

std::string rm_dots(float& f){
  std::string str = std::to_string(f);
  std::string chars = ".";
  for (char c: chars) str.erase(std::remove(str.begin(), str.end(), c), str.end());
  return str;
}


int main(int argc, char **argv){

  // Setup ROS
  ros::init(argc, argv, "publisher");
  ros::NodeHandle n;
  ros::Time startit = ros::Time::now();

  //
  std::string res, abs;
  n.getParam("/aufloesung", res);
  n.getParam("/abstand", abs);


  // Some data i will specify later
  float intervall = 1.2f;
  float resolution = 0.2f;
  size_t nthreads = 1;
  

  ros::AsyncSpinner spinner(1); // Never !! change this !
  spinner.start();
  ros::Rate loop_rate(1);

  std::vector<tf2::Vector3> point_list;
  std::vector<std::pair<geometry_msgs::Pose, bool>> all_poses, test;
  std::vector<std::thread> threads;

  // std::ofstream file(ros::package::getPath("reachability") + "/maps/reach/" + "RM_" + rm_dots(intervall) + "_" + rm_dots(resolution) + ".yaml");
  
  // Dictionary structure 
  
  cube_dec(intervall, resolution, point_list);
  generate_poses(point_list, all_poses, resolution);

  ROS_INFO("%li ^{0}p", point_list.size());
  ROS_INFO("%li ^{0}T_{Endeff}", all_poses.size());

  point_list.clear();
  point_list.push_back(tf2::Vector3(0,0,0));

  robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
  robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
  const robot_state::JointModelGroup* jmg = kinematic_model->getJointModelGroup("arm");
  robot_state::RobotStatePtr kinematic_state(new robot_state::RobotState(kinematic_model));


  std::vector<std::vector<std::pair<geometry_msgs::Pose, bool>>> partial_poses = split(all_poses, nthreads);
  ROS_INFO("%li", partial_poses.size());
  for (uint64_t i = 0; i < nthreads; i++) threads.push_back(std::thread(f_kin, std::ref(partial_poses[i]), kinematic_state, jmg));
  for (std::thread& t : threads) if(t.joinable()) t.join();
  
  double dif = ros::Duration( ros::Time::now() - startit).toSec();

  generate_poses(point_list, test, resolution);

  YAML::Node node;

  node["description"]["system"] = "Franka Emika 'Panda'";
  node["description"]["resolution"] = resolution;
  node["description"]["dimension"] = std::to_string(intervall) + " " + std::to_string(intervall) + " " + std::to_string(intervall) + " (m)";
  node["description"]["timestamp"] = std::to_string(dif) + " sec";

  std::ofstream file(ros::package::getPath("mtc") + "/maps/dummy.yaml");
  
  for (std::vector<std::pair<geometry_msgs::Pose, bool>>& poses_vec : partial_poses){
    for(std::pair<geometry_msgs::Pose, bool>& pair : poses_vec){
      if (pair.second) {
        node["data"].push_back(std::string(
            std::to_string(pair.first.position.x) + " " 
          + std::to_string(pair.first.position.y) + " " 
          + std::to_string(pair.first.position.z) + " "
          + std::to_string(pair.first.orientation.x) + " " 
          + std::to_string(pair.first.orientation.y) + " " 
          + std::to_string(pair.first.orientation.z) + " " 
          + std::to_string(pair.first.orientation.w) 
          ));
      }
    }
  }
  file << node;
  ros::shutdown();

  while (ros::ok()){
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}