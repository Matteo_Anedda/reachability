#include "../include/task_viewer.h"

std::vector<std::vector<std::string>> opkette;
std::vector<int> splits;
int id = 0;
std::string task_name;



Marker makeBox( InteractiveMarker &msg, const std_msgs::ColorRGBA& col){
  Marker marker;
  marker.type = Marker::CUBE;
  marker.scale.x = msg.scale * 0.02;
  marker.scale.y = msg.scale * 0.02;
  marker.scale.z = msg.scale * 0.2;
  marker.color = col;

  return marker;
}

InteractiveMarkerControl& makeBoxControl( InteractiveMarker &msg, std_msgs::ColorRGBA& col){
  InteractiveMarkerControl control;
  control.always_visible = true;

  control.markers.push_back( makeBox(msg, col) );
  msg.controls.push_back( control );
  return msg.controls.back();
}


void frameCallback(const ros::TimerEvent&)
{
  static uint32_t counter = 0;

  static tf::TransformBroadcaster br;

  tf::Transform t;

  ros::Time time = ros::Time::now();

  t.setOrigin(tf::Vector3(0.0, 0.0, sin(float(counter)/140.0) * 2.0));
  t.setRotation(tf::Quaternion(0.0, 0.0, 0.0, 1.0));
  br.sendTransform(tf::StampedTransform(t, time, "base_link", "moving_frame"));

  t.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  t.setRotation(tf::createQuaternionFromRPY(0.0, float(counter)/140.0, 0.0));
  br.sendTransform(tf::StampedTransform(t, time, "base_link", "rotating_frame"));

  counter++;
}


void processFeedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
  server->applyChanges();
}

void make6DofMarker(visualization_msgs::InteractiveMarker& int_marker){
  // insert a box
  InteractiveMarkerControl control;

  tf2::Quaternion a(1,0,0,1), b(0,1,0,1), c(0,0,1,1);
  a.normalize();
  b.normalize();
  c.normalize();

  control.orientation.x= a.getX();
  control.orientation.y= a.getY();
  control.orientation.z= a.getZ();
  control.orientation.w= a.getW();
  control.name = "rotate_x";
  control.interaction_mode = InteractiveMarkerControl::ROTATE_AXIS;
  int_marker.controls.push_back(control);
  control.name = "move_x";
  control.interaction_mode = InteractiveMarkerControl::MOVE_AXIS;
  int_marker.controls.push_back(control);

  control.orientation.x= b.getX();
  control.orientation.y= b.getY();
  control.orientation.z= b.getZ();
  control.orientation.w= b.getW();
  control.name = "rotate_z";
  control.interaction_mode = InteractiveMarkerControl::ROTATE_AXIS;
  int_marker.controls.push_back(control);
  control.name = "move_z";
  control.interaction_mode = InteractiveMarkerControl::MOVE_AXIS;
  int_marker.controls.push_back(control);

  control.orientation.x= c.getX();
  control.orientation.y= c.getY();
  control.orientation.z= c.getZ();
  control.orientation.w= c.getW();
  control.name = "rotate_y";
  control.interaction_mode = InteractiveMarkerControl::ROTATE_AXIS;
  int_marker.controls.push_back(control);
  control.name = "move_y";
  control.interaction_mode = InteractiveMarkerControl::MOVE_AXIS;
  int_marker.controls.push_back(control);

  server->insert(int_marker);
  server->setCallback(int_marker.name, &processFeedback);
}


void add_coop_node( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback ){
  // 0 add node
  // 1 manipulate coop node
  // 2 splits the task 
  // 3 store points into yaml file
  InteractiveMarker int_marker;
  tf::Point p(0,0,0);
  std_msgs::ColorRGBA col;
  col.a = col.r = 1;
  server->get(opkette.back().back(), int_marker);
  int_marker.controls[0].markers.front().color = col;
  server->insert(int_marker);
  server->setCallback(int_marker.name, &processFeedback);

  std::vector<std::string> str;
  str.push_back(int_marker.name);
  opkette.push_back(str);

  menu_handler.setVisible(all_entrys[1], false);
  menu_handler.setVisible(all_entrys[2], false);
  menu_handler.setVisible(all_entrys[3], false);


  menu_handler.apply( *server, "glied_0" );
  server->applyChanges();
}

void add_node( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback ){
  // 0 add node
  // 1 manipulate coop node
  // 2 splits the task 
  // 3 store points into yaml file
  InteractiveMarker int_marker;
  tf::Point p(0,0,0);
  std_msgs::ColorRGBA col;
  col.a = col.b = 1;
  int_marker.header.frame_id = "base_link";
  tf::pointTFToMsg(p, int_marker.pose.position);
  int_marker.scale = 1;
  int_marker.name = "glied_" + std::to_string(id);
  int_marker.description = int_marker.name;
  makeBoxControl(int_marker, col);
  int_marker.controls[0].interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_3D;
  make6DofMarker(int_marker);
  opkette.back().push_back(int_marker.name);
  id++;

  if (opkette.back().size() != 1){
    menu_handler.setVisible(all_entrys[1], true);
    menu_handler.setVisible(all_entrys[2], true);
    if (opkette.size() > 1) menu_handler.setVisible(all_entrys[3], true);
  } else {
    menu_handler.setVisible(all_entrys[1], false);
    menu_handler.setVisible(all_entrys[2], false);
    menu_handler.setVisible(all_entrys[3], false);
  }
  
  menu_handler.apply( *server, "glied_0" );
  server->applyChanges();
}

void split_here( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback ){
  // 0 add node
  // 1 manipulate coop node
  // 2 splits the task 
  // 3 store points into yaml file

  InteractiveMarker int_marker;
  std_msgs::ColorRGBA col;
  tf::Point p(0,0,0);
  col.a = col.g = 1;
  int_marker.header.frame_id = "base_link";
  tf::pointTFToMsg(p, int_marker.pose.position);
  int_marker.scale = 1;
  int_marker.name = "glied_" + std::to_string(id);
  int_marker.description = int_marker.name;
  makeBoxControl(int_marker, col);
  int_marker.controls[0].interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_3D;
  make6DofMarker(int_marker);

  std::vector<std::string> str;
  str.push_back(int_marker.name);

  splits.push_back(opkette.size());
  opkette.push_back(str);
  id++;

  menu_handler.setVisible(all_entrys[1], false);
  menu_handler.setVisible(all_entrys[2], false);
  menu_handler.setVisible(all_entrys[3], false);
  menu_handler.setVisible(all_entrys[4], true);

  menu_handler.apply( *server, "glied_0" );
  server->applyChanges();
}

void write_yaml( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback ){
  std::ofstream o(ros::package::getPath("reachability") + "/task/" + task_name + ".yaml");
  InteractiveMarker int_marker;
  YAML::Node node;

  node["Name_der_Aufgabe"] = task_name;

  int obj_count = 0;
  for (int i = 0; i < opkette.size(); i++){
    for (int& p : splits) if( p == i)obj_count++;
    for (std::string& str : opkette[i]){
      server->get(str, int_marker);
      node["Aufgabenbeschreibung"]["object" + std::to_string(obj_count)].push_back(std::string(std::to_string(int_marker.pose.position.x) + " " + std::to_string(int_marker.pose.position.y) + " " + std::to_string(int_marker.pose.position.z)));
    }
  }
  o << node;
}

void initMenu(std::vector<interactive_markers::MenuHandler::EntryHandle> &all_entrys){
  ROS_INFO("------------Init-Menu------------");
  ROS_INFO("Momentan ist das Menu nur über das erste Kettenglied (grün, ID=0) aufrufbar. Je nach Zustand der Kette, sind Teile des Menüs nicht versteckt oder offen.");
  ROS_INFO("(Option) Nächstes Glied: Zu jedem Zeitpunkt ist das Hinzufügen eines Gliedes an das zuletzt erzeugte Glied möglich.");
  ROS_INFO("(Option) Hier schneiden: Ab 2 Gliedern einer Operationskette beziehungsweise Teilkette ist ein Schnitt möglich, welcher die Kette in 2 disjunkte Teilketten teilt, zusätzlich wird ein weiteres grünes Glied erzeugt und initiiert den Sart der nächsten Kette.");
  ROS_INFO("(Option) Hier Kooperationsglied: Ab 2 Gliedern pro Operationskette beziehungsweise Teilkette ist ein Kooperationspunkt möglich, dies ist an der Farbe des letzten Gliedes erkennbar. Es wird kein neues Glied initiiert, sondern das letzte Gleid ist gleichzeitig erstes Glied der neuen Teilkette.");
  ROS_INFO("(Option) Generiere Aufgabenbeschreibung: Ab mehreren Teilketten mit >1 Gliedern ist die Formulierung der Aufgabenbeschreibung möglich.");


  all_entrys.push_back(menu_handler.insert( "Nächstes Glied", &add_node)); // 0 add node
  all_entrys.push_back(menu_handler.insert( "Hier Kooperationsglied", &add_coop_node)); // 1 manipulate coop node
  all_entrys.push_back(menu_handler.insert( "Hier schneiden", &split_here)); // 2 splits the task 
  all_entrys.push_back(menu_handler.insert( "Generiere Aufgabenbeschreibung", &write_yaml)); // 3 store points into yaml file
  
  menu_handler.setVisible(all_entrys[0], true);
  menu_handler.setVisible(all_entrys[1], false);
  menu_handler.setVisible(all_entrys[2], false);
  menu_handler.setVisible(all_entrys[3], false);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "task_builder");
  ros::NodeHandle n;
  n.getParam("/task_name", task_name);
  
  ROS_INFO("------------Task-Viewer------------");
  ROS_INFO("Dieses Programm generiert ein 'Aufgabenname.yaml' File, welcher die Aufgabenbeschreibung, in Form einer Operationskette, des Multi-Roboter Arbeitsplatzes dokumentiert. Diese Startet mit dem erste Glied");

  ROS_INFO("Name der Aufgabe: %s", task_name.c_str());


  ros::Timer frame_timer = n.createTimer(ros::Duration(0.01), frameCallback);

  server.reset( new interactive_markers::InteractiveMarkerServer("task_builder","",false) );

  initMenu(all_entrys);

  InteractiveMarker int_marker;
  std_msgs::ColorRGBA col;
  tf::Point p(0,0,0);
  col.a = col.g = 1;
  int_marker.header.frame_id = "base_link";
  tf::pointTFToMsg(p, int_marker.pose.position);
  int_marker.scale = 1;
  int_marker.name = "glied_" + std::to_string(id);
  int_marker.description = int_marker.name;
  makeBoxControl(int_marker, col);
  int_marker.controls[0].interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_3D;
  make6DofMarker(int_marker);

  std::vector<std::string> str;
  str.push_back(int_marker.name);
  opkette.push_back(str);
  id++;
  
  menu_handler.apply( *server, "glied_0" );
  server->applyChanges();

  ros::spin();

  server.reset();
}