#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/String.h"
#include "visualization_msgs/InteractiveMarker.h"
#include <tf/transform_broadcaster.h>
#include <regex>

#include "octomap/octomap.h"
#include <pcl/point_cloud.h>
#include <pcl/octree/octree.h>

#include "geometry_msgs/Vector3.h"
#include "moveit/move_group_interface/move_group_interface.h"
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <tf2/LinearMath/Transform.h>
#include "math.h" 
#include <fstream>
#include "yaml-cpp/yaml.h"
#include <xmlrpcpp/XmlRpc.h>

void task_ring(std::vector<tf2::Quaternion>& quat ){
  const double DELTA = M_PI / 4.0f;
  for (double phi = 0; phi < 2*M_PI; phi += DELTA){
    tf2::Quaternion rot;
    rot.setRPY(0, (M_PI / 2), phi);
    rot.normalize();
    quat.push_back(rot);
  }
}

void task_sphere(std::vector<tf2::Quaternion>& quat ){
  const double DELTA = M_PI / 4.0f;
  for (double phi = 0; phi < 2*M_PI; phi += DELTA){
    for (double theta = 0; theta <= M_PI*0.5; theta += DELTA){
      tf2::Quaternion rot;
      rot.setRPY(0, ((M_PI / 2) + theta), phi);
      rot.normalize();
      quat.push_back(rot);
    }
  }
}

std::vector<pcl::PointXYZ> generateBoxTree(const tf2::Vector3& origin, float diameter, float resolution){
  unsigned char depth = 16;
  std::vector<pcl::PointXYZ> box; 
  octomap::OcTree* tree = new octomap::OcTree(resolution/2);
  for (float x = origin.getX() - diameter * 1.5 ; x <= origin.getX() + diameter * 1.5 ; x += resolution){
    for (float y = origin.getY() - diameter * 1.5 ; y <= origin.getY() + diameter * 1.5 ; y += resolution){
      for (float z = origin.getZ() - diameter * 1.5 ; z <= origin.getZ() + diameter * 1.5 ; z += resolution){
        octomap::point3d point(x,y,z);
        tree->updateNode(point, true);
      }
    }
  }

  for (octomap::OcTree::leaf_iterator it = tree->begin_leafs(depth), end = tree->end_leafs(); it != end; ++it){
    pcl::PointXYZ searchPoint(it.getCoordinate().x(), it.getCoordinate().y(), it.getCoordinate().z());
    box.push_back(searchPoint);
  }

  return box;
}

std::vector<pcl::PointXYZ> generateGround(const tf2::Vector3& origin, float diameter, float resolution){
  std::vector<pcl::PointXYZ> ground_plane; 
  resolution/= 2;

  for (float x = origin.getX() - diameter * 1.5; x <= origin.getX() + diameter * 1.5; x += resolution){
    for (float y = origin.getY() - diameter * 1.5; y <= origin.getY() + diameter * 1.5; y += resolution){
      pcl::PointXYZ point(x,y,0);
      ground_plane.push_back(point);     
    }
  }
  return ground_plane;
}

pcl::PointCloud< pcl::PointXYZ >::Ptr vector_to_cloud(std::vector<pcl::PointXYZ>& vector){
  pcl::PointCloud< pcl::PointXYZ >::Ptr task_voxel(new pcl::PointCloud< pcl::PointXYZ >);
  for(pcl::PointXYZ& point : vector)
    task_voxel->push_back(point);
  
  return task_voxel;
}

void chaining(std::vector<std::vector<tf2::Vector3>>& ok, std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total){
  for(std::vector<tf2::Vector3>& tk: ok){
    tf2::Vector3 temp = tk.front();
    std::vector<tf2::Vector3> tk_g;
    tk_g.push_back(temp);

    std::vector<std::vector<tf2::Vector3>> chains; 

    for (int i = 1; i < tk.size(); i++){
      if(tf2::tf2Distance(temp, tk[i])==0){
        chains.push_back(tk_g);
        tk_g.clear();
      }
      tk_g.push_back(tk[i]);
      temp = tk[i];
    }
    chains.push_back(tk_g);
    ok_total.push_back(chains);
  }

  // Debug 
  for(std::vector<std::vector<tf2::Vector3>>& tk: ok_total) for(std::vector<tf2::Vector3>& k : tk) {
    ROS_INFO("-------");
    for(tf2::Vector3& g : k) ROS_INFO("%f %f %f", g.getX(), g.getY(), g.getZ());
  }
}

void marker_per_glied(visualization_msgs::MarkerArray& marker_arr, 
  std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>& P_total,
  std::vector<std::vector<float>>& index_total){

  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;

  
  for(int i = 0; i < P_total.size(); i++){
    marker_template.ns= "Teilkette " + std::to_string(i);
    for(int l = 0; l < P_total[i]->points.size(); l++){
      geometry_msgs::Point p;
      std_msgs::ColorRGBA col;
      col.a = 1;
      float index = index_total[i][l];
      col.g = index;

      p.x = P_total[i]->points[l].x;
      p.y = P_total[i]->points[l].y;
      p.z = P_total[i]->points[l].z;
      marker_template.points.push_back(p);
      marker_template.colors.push_back(col);
    }
    marker_arr.markers.push_back(marker_template);
    marker_template.points.clear();
    marker_template.colors.clear();
  }
}

void marker_per_intersection(visualization_msgs::MarkerArray& marker_arr, 
  std::vector<std::pair<pcl::PointXYZ, float>>& intersections){
  
  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;
  marker_template.ns= "Teilkette";

  for(std::pair<pcl::PointXYZ, float>& voxel : intersections){
    geometry_msgs::Point p;
    std_msgs::ColorRGBA col;
    col.a = 1;
    col.g = voxel.second;


    p.x = voxel.first.x;
    p.y = voxel.first.y;
    p.z = voxel.first.z;
    marker_template.points.push_back(p);
    marker_template.colors.push_back(col);
  }
  marker_arr.markers.push_back(marker_template);
  marker_template.points.clear();
  marker_template.colors.clear();
}


void marker_intersection_to_ground(visualization_msgs::MarkerArray& marker_arr, 
  std::vector<std::pair<pcl::PointXYZ, float>>& ground){
  
  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;
  marker_template.ns= "Teilkette auf Grund";
  for(std::pair<pcl::PointXYZ, float>& voxel : ground) {
    geometry_msgs::Point p;
    std_msgs::ColorRGBA col;
    col.a = 1;
    col.g = voxel.second;


    p.x = voxel.first.x;
    p.y = voxel.first.y;
    p.z = voxel.first.z;
    marker_template.points.push_back(p);
    marker_template.colors.push_back(col);
  }
  marker_arr.markers.push_back(marker_template);
  marker_template.points.clear();
  marker_template.colors.clear();
  
}

void intersection_filter(std::vector<std::pair<pcl::PointXYZ, float>>& intersections,
  std::vector<tf2::Vector3>& ok_total, 
  std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>& voxel_dictionary){

  for(std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>& voxel : voxel_dictionary){
    if(voxel.second.size() == ok_total.size()){
      float index_accumulate = 0;
      for (std::pair<int, float>& index : voxel.second) index_accumulate += index.second;
      index_accumulate/= static_cast<float>(ok_total.size());
      intersections.push_back(std::make_pair(voxel.first, index_accumulate));
    }
  }
}

void marker_voxel(visualization_msgs::MarkerArray& marker_arr,
  std::vector<tf2::Vector3>& ok_total, 
  std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>& voxel_dictionary){

  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;

  for(int k = 0; k < ok_total.size(); k++) {
    marker_template.ns = "Voxelisierung " + std::to_string(k);
    for(int i = 0; i < voxel_dictionary.size(); i++){
      for(std::pair<int, float>& voxel : voxel_dictionary[i].second)
        if(voxel.first == k){
          geometry_msgs::Point point;
          std_msgs::ColorRGBA col;
          point.x = voxel_dictionary[i].first.x;
          point.y = voxel_dictionary[i].first.y;
          point.z = voxel_dictionary[i].first.z;
          col.a = 1;
          col.g = voxel.second;
          
          marker_template.points.push_back(point);
          marker_template.colors.push_back(col);
        }
      marker_arr.markers.push_back(marker_template);
      marker_template.points.clear();
      marker_template.colors.clear();
    }
  }
}

void marker_voxel_ground(visualization_msgs::MarkerArray& marker_arr,
  std::vector<tf2::Vector3>& ok_total, 
  std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>& voxel_dictionary,
  float& resolution,
  std::vector<pcl::PointXYZ>& ground){

  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;

  for(int i = 0; i < voxel_dictionary.size(); i++){
    for(int k = 0; k < ok_total.size(); k++) {
      marker_template.ns = "Voxelisierung Boden " + std::to_string(k);
      std::vector<pcl::PointXYZ> points;
      std::vector<float> index;
      for(std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>> voxel : voxel_dictionary) for(std::pair<int, float>& pair : voxel.second)
        if(pair.first == k){
          points.push_back(voxel.first);
          index.push_back(pair.second);
        }
      for (pcl::PointXYZ& p : ground){
        pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
        octree.setInputCloud(vector_to_cloud(points));
        octree.addPointsFromInputCloud();
        double min_x, min_y, min_z, max_x, max_y, max_z;
        octree.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
        bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);     
        if (isInBox && octree.isVoxelOccupiedAtPoint(p)) {
          std::vector< int > pointIdxVec;
          if (octree.voxelSearch(p, pointIdxVec)){
            geometry_msgs::Point point;
            std_msgs::ColorRGBA col;
            point.x = p.x;
            point.y = p.y;
            point.z = p.z;
            float index_accumulate = 0;
            for (int& in : pointIdxVec) index_accumulate += index[in];
            index_accumulate /= static_cast<float>(pointIdxVec.size());
            col.a = 1;
            col.g = index_accumulate;
            marker_template.points.push_back(point);
            marker_template.colors.push_back(col);
          }
        }
      }
      marker_arr.markers.push_back(marker_template);
      marker_template.points.clear();
      marker_template.colors.clear();
    }
  }
}

int main(int argc, char **argv){

  float resolution = 0.125f;
  float max_abstand = 2.0f;
  
  std::regex reg("\\s+");


  ros::init(argc, argv, "map_analyse");
  ros::NodeHandle n;
  
  std::vector<float> metrik;
  std::vector<std::string> irm;

  XmlRpc::XmlRpcValue task;
  std::string name;
  std::string rs;


  n.getParam("/Metrik", metrik);
  n.getParam("/Endeffektor_Pose", irm);

  n.getParam("/Aufgabenbeschreibung", task);
  n.getParam("/Name_der_Aufgabe", name);
  n.getParam("/Referenzsystem", rs);



  std::vector<std::vector<tf2::Vector3>> ok;

  for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it = task.begin(); it != task.end(); ++it){
    std::vector<std::string> tk_s;
    std::vector<tf2::Vector3> tk;

    n.getParam("/Aufgabenbeschreibung/" + it->first, tk_s);
    for (std::string& kg : tk_s){;
      std::sregex_token_iterator to_it(kg.begin(), kg.end(),reg,-1);
      std::vector<std::string> temp{to_it, {}};
      tk.push_back(tf2::Vector3(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2])));
    }
    ok.push_back(tk);
  }

  // all for one
  std::vector<tf2::Vector3> ok_total; 
  for(std::vector<tf2::Vector3>& tk : ok) for(tf2::Vector3& kg : tk) ok_total.push_back(kg);



  ros::AsyncSpinner spinner(1); // Never !! change this !
  spinner.start();
  ros::Rate loop_rate(1);

  // set a origin point
  tf2::Vector3 p(0,0,0);

  visualization_msgs::MarkerArray marker_arr;
  
  std::vector<std::vector<float>> index_total;
  std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr> P_total;

  for (tf2::Vector3& kg : ok_total){
    pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_task(new pcl::PointCloud< pcl::PointXYZ >);
    std::vector<float> index;
    tf2::Transform task;
    task.setOrigin(kg);
    for (int i = 0; i < irm.size(); i++){
      tf2::Transform trans, base;
      std::sregex_token_iterator to_it(irm[i].begin(), irm[i].end(),reg,-1);
      std::vector<std::string> temp{to_it, {}};
      tf2::Vector3 invvec(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2]));
      tf2::Quaternion invrot(std::stof(temp[3]), std::stof(temp[4]), std::stof(temp[5]), std::stof(temp[6]));
      
      trans.setOrigin(invvec);
      trans.setRotation(invrot);

      std::vector<tf2::Quaternion> sphere_orientations; 
      task_sphere(sphere_orientations);
      for(tf2::Quaternion& rot: sphere_orientations){
        task.setRotation(rot);
        base = task* trans;

        tf2::Vector3 basevec = base.getOrigin();
        tf2::Quaternion baserot = base.getRotation().normalized();

        pcl::PointXYZ point(basevec.getX(), basevec.getY(), basevec.getZ());
        cloud_task->push_back(point);
        index.push_back(metrik[i]);
      }
    }
    P_total.push_back(cloud_task);
    index_total.push_back(index);
  }

  ROS_INFO("Alle P_total pro Kettenglied berechnet.");
  ROS_INFO("Anzahl: %li", P_total.size());

  // prepare grid structures for voxelisation
  std::vector<pcl::PointXYZ> spCenter = generateBoxTree(p, max_abstand, resolution);
  std::vector<pcl::PointXYZ> grCenter = generateGround(p, max_abstand, resolution);
  ROS_INFO("Arbeitsraum erstellt, starte Voxelisierung.");

  std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>> voxel_dictionary;

  ros::Time startit = ros::Time::now();
  
  for(pcl::PointXYZ& p : spCenter){
    std::vector<std::pair<int, float>> pair;
    for(int i = 0; i < P_total.size();i++){
      pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
      octree.setInputCloud(P_total[i]);
      octree.addPointsFromInputCloud();
      double min_x, min_y, min_z, max_x, max_y, max_z;
      octree.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);

      bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);     
      if (isInBox && octree.isVoxelOccupiedAtPoint(p)) {
        std::vector< int > pointIdxVec;
        if (octree.voxelSearch(p, pointIdxVec)){
          float index_accumulate = 0;
          for (int& index : pointIdxVec) index_accumulate += index_total[i][index];
          index_accumulate/= static_cast<float>(pointIdxVec.size());
          pair.push_back(std::make_pair(i, index_accumulate));
          }
      }
    }
    if(!pair.empty()) voxel_dictionary.push_back(std::make_pair(p, pair));
  }

  ROS_INFO("%li", voxel_dictionary.size());
  ROS_INFO("Voxelisierung abgeschlossen");

  std::vector<std::pair<pcl::PointXYZ, float>> intersections;
  intersection_filter(intersections, ok_total, voxel_dictionary);

  ROS_INFO("%li", intersections.size());

  ROS_INFO("Schnittmengenanalyse abgeschlossen");
  ROS_INFO("Initialisiere Baseisposition Analyse");

  std::vector<std::pair<pcl::PointXYZ, float>> ground_voxel;
  std::vector<pcl::PointXYZ> points;
  std::vector<float> index;
  for (auto it = std::make_move_iterator(intersections.begin()), end = std::make_move_iterator(intersections.end()); it != end; ++it){
    points.push_back(std::move(it->first));
    index.push_back(std::move(it->second));
  }

  
  pcl::PointCloud< pcl::PointXYZ >::Ptr voxeled_task = vector_to_cloud(points);
  pcl::octree::OctreePointCloudSearch< pcl::PointXYZ > groundOctree(resolution);
  groundOctree.setInputCloud(voxeled_task);
  groundOctree.addPointsFromInputCloud();
  for(pcl::PointXYZ& p : grCenter){

    double min_x, min_y, min_z, max_x, max_y, max_z;
    groundOctree.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
    bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);
    if (isInBox && groundOctree.isVoxelOccupiedAtPoint(p)) {
      std::vector< int > pointIdxVec;
      if (groundOctree.voxelSearch(p, pointIdxVec)){
        ROS_INFO("hallo");

        float index_accumulate = 0;
        for (int& in : pointIdxVec) index_accumulate += index[in];
        ground_voxel.push_back(std::make_pair(p, index_accumulate/static_cast<float>(pointIdxVec.size())));
      }
    }
  }

  /*
  ROS_INFO("Ground filter");
  std::vector<std::vector<bool>> ground_voxel_filter;
  float d = 0.25;
  for(std::pair<pcl::PointXYZ, float>& pair : ground_voxel){
    tf2::Vector3 goal(pair.first.x, pair.first.y, pair.first.z);
    std::vector<bool> filtere_voxel_point;
    for(int k = 0; k < ok_total.size(); k++){
      tf2::Vector3 task = ok_total[k];
      task.setZ(0);
      (tf2::tf2Distance(goal, task) >= d ) ? filtere_voxel_point.push_back(true) :filtere_voxel_point.push_back(false);
      }
    ground_voxel_filter.push_back(filtere_voxel_point);
  }
  ROS_INFO("Basis Ermittlung");
  
  std::vector<std::pair<pcl::PointXYZ, float>> goal_tree;
  std::vector<std::pair<pcl::PointXYZ, float>> filter;
  for(int k = 0; k < ground_voxel.size(); k++)
    if(std::all_of(ground_voxel_filter[k].begin(), ground_voxel_filter[k].end(),[](bool v) {return v;})) filter.push_back(ground_voxel[k]);
  points.clear();
  index.clear();
  for (auto it = std::make_move_iterator(filter.begin()), end = std::make_move_iterator(filter.end()); it != end; ++it){
    points.push_back(std::move(it->first));
    index.push_back(std::move(it->second));
  }
  */
  points.clear();
  index.clear();

  for (auto it = std::make_move_iterator(ground_voxel.begin()), end = std::make_move_iterator(ground_voxel.end()); it != end; ++it){
    points.push_back(std::move(it->first));
    index.push_back(std::move(it->second));
  }
  std::pair<pcl::PointXYZ, float> pair;
  int in = std::distance(index.begin(), std::max_element(index.begin(), index.end()));
  pair.first = points[in];
  pair.second = index[in];
  
  double dif = ros::Duration( ros::Time::now() - startit).toSec();
  ROS_INFO("Positionen für den Roboter kalkuliert");
  std::ofstream o(ros::package::getPath("reachability") + "/Positionsanalyse/Single_Base_" + name + ".yaml");
  YAML::Node node;
  node["Referenzsystem"] = rs;
  node["Name_der_Aufgabe"] = name;
  node["Roboter_Anzahl"] = 1;
  node["Map_Aufloesung"] = resolution;
  node["Zeit"] = dif;

  int count = 1;
  
  for(tf2::Vector3& vec : ok_total)
    node["Move_Groups"]["objekt" + std::to_string(0)]["panda_arm"+ std::to_string(1)].push_back(std::string(
        std::to_string(vec.getX()) + " " 
      + std::to_string(vec.getY()) + " " 
      + std::to_string(vec.getZ())));
  node["Basispositionen"]["panda_arm"+ std::to_string(1)] = std::string(
      std::to_string(pair.first.x) + " " 
      +  std::to_string(pair.first.y) + " " 
      +  std::to_string(pair.first.z));
  
  o << node;

  marker_per_glied(marker_arr, P_total, index_total);
  marker_voxel(marker_arr, ok_total, voxel_dictionary);
  //marker_voxel_ground(marker_arr, ok_total, voxel_dictionary, resolution, grCenter);
  marker_per_intersection(marker_arr, intersections);
  marker_intersection_to_ground(marker_arr, ground_voxel);



  ros::Publisher marker_pub = n.advertise< visualization_msgs::MarkerArray >("visualization_marker_array", 1);
  while (ros::ok()){
    marker_pub.publish(marker_arr);
    ros::spinOnce();
    loop_rate.sleep();
  }
}