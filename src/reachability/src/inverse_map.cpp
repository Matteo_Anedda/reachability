#include "../include/inverse_map.h"

int main(int argc, char **argv){

  // Setup ROS
  ros::init(argc, argv, "publisher");
  ros::NodeHandle n;
  std::vector<std::string> all_poses;
  std::vector<bool> f_kin;
  std::string name;
  std::regex reg("\\s+");
  
  ros::Time startit = ros::Time::now();
  float resolution;
  float intervall;
  float OR_total;

  n.getParam("/Endeffektor_Pose", all_poses);
  n.getParam("/IK_Loesung", f_kin);
  n.getParam("/Map_Name", name);
  n.getParam("/Aufloesung", resolution);
  n.getParam("/Intervall", intervall);
  n.getParam("/OR_total", OR_total);


  ros::Rate loop_rate(1);

  // Take Reachability map from json data
  std::vector<std::pair<tf2::Vector3, float>> reachability_info;
  tf2::Vector3 p(0,0,0);

  std::vector<geometry_msgs::Pose> all_inv;
  std::vector<float> index;

  int entry_count = OR_total;
  ROS_INFO("%i", entry_count);

  std::vector<bool> b;
  for (int i = 0; i < all_poses.size(); i++){
      b.push_back(f_kin[i]);
      if (b.size() == entry_count){
        index.push_back(static_cast<float>(std::count(b.begin(),b.end(), true))/entry_count);
        b.clear();
      }
  }

  // max
  std::vector<float> max_index;
  for(float& ind : index){
    for(int i = 0; i < entry_count; i++){
      max_index.push_back(ind);
    }
  }

  index.clear();
  // do inverse transformations
  for (int i = 0; i< all_poses.size(); i++){
    if (f_kin[i]){
      std::sregex_token_iterator to_it(all_poses[i].begin(), all_poses[i].end(),reg,-1);
      std::vector<std::string> temp{to_it, {}};
      
      tf2::Transform trans;
      tf2::Transform inv_trans;

      tf2::Quaternion q(std::stof(temp[3]), std::stof(temp[4]), std::stof(temp[5]), std::stof(temp[6]));
      tf2::Vector3 v(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2]));

      trans.setRotation(q);
      trans.setOrigin(v);
      inv_trans = trans.inverse();
      tf2::Vector3 inv_trans_origin = inv_trans.getOrigin();
      tf2::Quaternion inv_trans_quat = inv_trans.getRotation().normalize();

      geometry_msgs::Pose pose;
      pose.position.x = inv_trans_origin.getX();
      pose.position.y = inv_trans_origin.getY();
      pose.position.z = inv_trans_origin.getZ();
      pose.orientation.x = inv_trans_quat.getX();
      pose.orientation.y = inv_trans_quat.getY();
      pose.orientation.z = inv_trans_quat.getZ();
      pose.orientation.w = inv_trans_quat.getW();

      all_inv.push_back(pose);
      index.push_back(max_index[i]);
    }
  } 

  std::ofstream o(ros::package::getPath("reachability") + "/maps/inverse/I" + name +".yaml");
  double dif = ros::Duration( ros::Time::now() - startit).toSec();
  
  YAML::Node node;
  node["Referenzsystem"] = "Franka Emika 'Panda'";
  node["Map_Name"] = "IRM_" + name;
  node["Aufloesung"] = resolution;
  node["Intervall"] = intervall;
  node["Zeit"] = dif;

  for (int i = 0; i < all_inv.size(); i++){
    node["Endeffektor_Pose"].push_back(std::string(
        std::to_string(all_inv[i].position.x) + " " 
      + std::to_string(all_inv[i].position.y) + " " 
      + std::to_string(all_inv[i].position.z) + " "
      + std::to_string(all_inv[i].orientation.x) + " " 
      + std::to_string(all_inv[i].orientation.y) + " " 
      + std::to_string(all_inv[i].orientation.z) + " " 
      + std::to_string(all_inv[i].orientation.w) 
      ));
    node["Metrik"].push_back(index[i]);
  }
  o << node;
  ROS_INFO("jetzt is fertig"); 
  while (ros::ok()){
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}