#include <ros/ros.h>
#include <ros/package.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <moveit_msgs/ApplyPlanningScene.h>
#include <regex>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>

#include <fstream>
#include <xmlrpcpp/XmlRpc.h>


// The circle constant tau = 2*pi. One tau is one rotation in radians.
const double tau = 2 * M_PI;

void task_ring(std::vector<tf2::Quaternion>& quat, std::vector<tf2::Vector3>& vecs,  tf2::Vector3& task){
  const double DELTA = M_PI / 4.0f;
  for (double phi = 0; phi < 2*M_PI; phi += DELTA){
    tf2::Quaternion rot;
    float x, y;
    x = 0.2 * cos(phi) * sin(0) + task.getX();
    y = 0.2 * sin(phi) * sin(0) + task.getY();
    vecs.push_back(tf2::Vector3(x,y,0));
    rot.setRPY(0, 0, phi);
    rot.normalize();
    quat.push_back(rot);
  }
}

void openGripper(trajectory_msgs::JointTrajectory& posture, std::string& id){
  for (int i = 1; i <= 2;i++){
    posture.joint_names.push_back(id + std::to_string(i));
  }
 
  posture.points.resize(1);
  posture.points[0].positions.resize(2);
  posture.points[0].positions[0] = 0.04;
  posture.points[0].positions[1] = 0.04;
  posture.points[0].time_from_start = ros::Duration(0.5);
}

void closedGripper(trajectory_msgs::JointTrajectory& posture, std::string& id){
  for (int i = 1; i <= 2;i++){
    posture.joint_names.push_back(id + std::to_string(i));
  }

  posture.points.resize(1);
  posture.points[0].positions.resize(2);
  posture.points[0].positions[0] = 0.00;
  posture.points[0].positions[1] = 0.00;
  posture.points[0].time_from_start = ros::Duration(0.5);

}

void pick(moveit::planning_interface::MoveGroupInterface& move_group, tf2::Vector3& pos, int& obj){
  std::vector<moveit_msgs::Grasp> grasps;
  std::string id;

  grasps.resize(1);
  grasps[0].grasp_pose.header.frame_id = "world";
  tf2::Quaternion orientation;
  orientation.setRPY(-tau / 4, -tau / 8, -tau / 4);
  grasps[0].grasp_pose.pose.orientation = tf2::toMsg(orientation);
  grasps[0].grasp_pose.pose.position.x = pos.getX() - 0.085f;
  grasps[0].grasp_pose.pose.position.y = pos.getY();
  grasps[0].grasp_pose.pose.position.z = pos.getZ();

  grasps[0].pre_grasp_approach.direction.header.frame_id = "panda_";
  grasps[0].pre_grasp_approach.direction.header.frame_id += move_group.getName().back();
  id = grasps[0].pre_grasp_approach.direction.header.frame_id +"_finger_joint";;
  grasps[0].pre_grasp_approach.direction.header.frame_id += "_link0";

  grasps[0].pre_grasp_approach.direction.vector.z= -1.0;
  grasps[0].pre_grasp_approach.min_distance = 0.1;
  grasps[0].pre_grasp_approach.desired_distance = 0.15;

  grasps[0].post_grasp_retreat.direction.header.frame_id = grasps[0].pre_grasp_approach.direction.header.frame_id;

  grasps[0].post_grasp_retreat.direction.vector.z = 1.0;
  grasps[0].post_grasp_retreat.min_distance = 0.1;
  grasps[0].post_grasp_retreat.desired_distance = 0.15;



  openGripper(grasps[0].pre_grasp_posture, id);
  closedGripper(grasps[0].grasp_posture,id);
  move_group.pick("object"+ std::to_string(obj), grasps);
}

void place(moveit::planning_interface::MoveGroupInterface& move_group, tf2::Transform& trans, int obj){
  tf2::Quaternion orientation = trans.getRotation();
  tf2::Vector3 pos = trans.getOrigin();


  std::vector<moveit_msgs::PlaceLocation> place_location;
  std::string id;

  place_location.resize(1);
  place_location[0].place_pose.header.frame_id = "world";


  orientation.setRPY(0, 0, -M_PI);  // A quarter turn about the z-axis
  place_location[0].place_pose.pose.orientation = tf2::toMsg(orientation);

  place_location[0].place_pose.pose.position.x = pos.getX();
  place_location[0].place_pose.pose.position.y = pos.getY();
  place_location[0].place_pose.pose.position.z = pos.getZ();

  place_location[0].pre_place_approach.direction.header.frame_id ="panda_";
  place_location[0].pre_place_approach.direction.header.frame_id+= move_group.getName().back();
  id = place_location[0].pre_place_approach.direction.header.frame_id +"_finger_joint";;
  place_location[0].pre_place_approach.direction.header.frame_id += "_link0";


  place_location[0].pre_place_approach.direction.vector.z = -1.0;
  place_location[0].pre_place_approach.min_distance = 0.0;
  place_location[0].pre_place_approach.desired_distance = 0.25;

  place_location[0].post_place_retreat.direction.header.frame_id = place_location[0].pre_place_approach.direction.header.frame_id;
  place_location[0].post_place_retreat.direction.vector.z = 1;
  place_location[0].post_place_retreat.min_distance = 0.0;
  place_location[0].post_place_retreat.desired_distance = 0.4;

  openGripper(place_location[0].post_place_posture, id);
  move_group.place("object" + std::to_string(obj), place_location);
}


void generate_scene(std::vector<moveit_msgs::CollisionObject>& collision_objects, std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total){
  tf2::Vector3 obj_dimension(0.02, 0.02, 0.2);

  std::vector<tf2::Vector3> obj_count;
  for(std::vector<std::vector<tf2::Vector3>>& tk : ok_total) for(std::vector<tf2::Vector3>& subk : tk) obj_count.insert(obj_count.end(), subk.begin(), subk.end());

  std::vector<tf2::Vector3> obj_count_clean;
  tf2::Vector3 temp = obj_count.front();
  obj_count_clean.push_back(temp);
  for(int i = 1; i < obj_count.size(); i++){
    if(tf2::tf2Distance(temp, obj_count[i])!=0) obj_count_clean.push_back(obj_count[i]);
    temp = obj_count[i];
  }

  collision_objects.resize(obj_count_clean.size() + ok_total.size());

  for (int i = 0; i <  ok_total.size(); i++ ){
    collision_objects[i].header.frame_id = "world";
    collision_objects[i].id = "object" + std::to_string(i);

    collision_objects[i].primitives.resize(1);
    collision_objects[i].primitives[0].type = collision_objects[1].primitives[0].BOX;
    collision_objects[i].primitives[0].dimensions.resize(3);
    collision_objects[i].primitives[0].dimensions[0] = obj_dimension.getX();
    collision_objects[i].primitives[0].dimensions[1] = obj_dimension.getY();
    collision_objects[i].primitives[0].dimensions[2] = obj_dimension.getZ();

    collision_objects[i].primitive_poses.resize(1);
    collision_objects[i].primitive_poses[0].position.x = ok_total[i].front().front().getX();
    collision_objects[i].primitive_poses[0].position.y = ok_total[i].front().front().getY();
    collision_objects[i].primitive_poses[0].position.z = ok_total[i].front().front().getZ();
    collision_objects[i].primitive_poses[0].orientation.w = 1.0;
  
    collision_objects[i].operation = collision_objects[i].ADD;
  }

  for (int i = 0; i < obj_count_clean.size(); i++){
    collision_objects[i+ok_total.size()].id = "table" + std::to_string(i);
    collision_objects[i+ok_total.size()].header.frame_id = "world";

    collision_objects[i+ok_total.size()].primitives.resize(1);
    collision_objects[i+ok_total.size()].primitives[0].type = collision_objects[0].primitives[0].BOX;
    collision_objects[i+ok_total.size()].primitives[0].dimensions.resize(3);
    collision_objects[i+ok_total.size()].primitives[0].dimensions[0] = 0.1;
    collision_objects[i+ok_total.size()].primitives[0].dimensions[1] = 0.1;
    collision_objects[i+ok_total.size()].primitives[0].dimensions[2] = obj_count_clean[i].getZ() - (obj_dimension.getZ()/2);


    collision_objects[i+ok_total.size()].primitive_poses.resize(1);
    collision_objects[i+ok_total.size()].primitive_poses[0].position.x = obj_count_clean[i].getX();
    collision_objects[i+ok_total.size()].primitive_poses[0].position.y = obj_count_clean[i].getY();
    collision_objects[i+ok_total.size()].primitive_poses[0].position.z = (obj_count_clean[i].getZ() - (obj_dimension.getZ()/2))*0.5f;
    collision_objects[i+ok_total.size()].primitive_poses[0].orientation.w = 1.0;

    collision_objects[i+obj_count_clean.size()].operation = collision_objects[i].ADD;
  }
}





int main(int argc, char** argv){
  ros::init(argc, argv, "panda_arm_pick_place");
  ros::NodeHandle nh;
  ros::Publisher collision_object_publisher = nh.advertise<moveit_msgs::CollisionObject>("collision_object", 1);
  ros::Publisher planning_scene_diff_publisher = nh.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
  ros::AsyncSpinner spinner(1);
  ros::WallDuration sleep_time(1.0);
  spinner.start();
  ros::Time startamdal = ros::Time::now();



  std::regex reg("\\s+");

  XmlRpc::XmlRpcValue aufgabenverteilung;
  nh.getParam("/Move_Groups", aufgabenverteilung);


  
  

  std::vector<std::vector<std::vector<tf2::Vector3>>> ok_total;
  for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it = aufgabenverteilung.begin(); it != aufgabenverteilung.end(); ++it){
    ROS_INFO("----");
    XmlRpc::XmlRpcValue list_of_mg = it->second;
    std::vector<std::vector<tf2::Vector3>> tk;
    for (XmlRpc::XmlRpcValue::ValueStruct::const_iterator it1 = list_of_mg.begin(); it1 != list_of_mg.end(); ++it1){
      std::vector<std::string> positions; 
      nh.getParam("/Move_Groups/" + it->first + "/" + it1->first, positions);
      std::vector<tf2::Vector3> pos_vec;
      for(std::string& str : positions){
        std::sregex_token_iterator to_it(str.begin(), str.end(),reg,-1);
        std::vector<std::string> temp{to_it, {}};
        pos_vec.push_back(tf2::Vector3(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2])));
      }
      tk.push_back(pos_vec);
    }
    ok_total.push_back(tk);
  }

  ROS_INFO("Bau der Szene");

  std::vector<moveit_msgs::CollisionObject> collision_objects;
  generate_scene(collision_objects, ok_total);

  for (moveit_msgs::CollisionObject& col_obj : collision_objects){
    collision_object_publisher.publish(col_obj);
    sleep_time.sleep();
  }


  moveit_msgs::PlanningScene planning_scene;
  planning_scene.robot_state.attached_collision_objects.clear();
  planning_scene.world.collision_objects.clear();
  planning_scene.world.collision_objects = collision_objects;
  planning_scene.is_diff = true;
  planning_scene_diff_publisher.publish(planning_scene);
  sleep_time.sleep();

  robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
  robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
  planning_scene::PlanningScene ps(kinematic_model);


  std::vector<moveit::planning_interface::MoveGroupInterface>  robots;
  int table_pos = 0;
  std::vector<std::string> robot_pos;

  for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it = aufgabenverteilung.begin(); it != aufgabenverteilung.end(); ++it)
    for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it1 = it->second.begin(); it1 != it->second.end(); ++it1){
      ROS_INFO("Call Robot %s", it1->first.c_str());
      robots.push_back(std::move(moveit::planning_interface::MoveGroupInterface(it1->first)));
    }
  
  std::string hallo;
  nh.getParam("/Basispositionen/panda_arm1" , hallo);
  robot_pos.push_back(hallo);
  nh.getParam("/Basispositionen/panda_arm2" , hallo);
  robot_pos.push_back(hallo);

  collision_detection::AllowedCollisionMatrix acm = ps.getAllowedCollisionMatrix();
  acm.setEntry("object0", "panda_1_leftfinger", true);
  acm.setEntry("object0" , "panda_1_rightfinger", true);
  acm.setEntry("object1", "panda_2_leftfinger", true);
  acm.setEntry("object1" , "panda_2_rightfinger", true);

  moveit_msgs::PlanningScene pls;
  acm.getMessage(pls.allowed_collision_matrix);
  pls.is_diff = true;
  planning_scene_diff_publisher.publish(pls);

  /*
  int i = 0;
  robots[0].setSupportSurfaceName("table"+ std::to_string(0));
  pick(robots[0], ok_total[0][0][0], i);
  robots[0].setSupportSurfaceName("table"+ std::to_string(1));
  pick(robots[0], ok_total[0][0][1], i);

  int table = 0;
  */
  double amd = ros::Duration( ros::Time::now() - startamdal).toSec();
  ROS_INFO("%f",amd);

  for(int i = 0; i< ok_total.size(); i++){
    ros::Time startit = ros::Time::now();
    for(int j = 0; j < ok_total[i].size();j++){
      for(int k = 0; k < (ok_total[i][j].size()-1);k++){
        tf2::Transform trans;
        tf2::Quaternion orientation;
        trans.setOrigin(ok_total[i][j][k+1]);

        std::sregex_token_iterator to_it(robot_pos[i+j].begin(), robot_pos[i+j].end(),reg,-1);
        std::vector<std::string> temp{to_it, {}};
        tf2::Vector3 robot_vec(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2]));
        (robot_vec.getX()<ok_total[i][j][k].getX()) ? orientation.setRPY(0,0,0) :orientation.setRPY(0,0,M_PI);
        trans.setRotation(orientation);

        robots[i+j].setSupportSurfaceName("table"+ std::to_string(table_pos));
        pick(robots[i+j], ok_total[i][j][k], i);
        table_pos++;
        robots[i+j].setSupportSurfaceName("table"+ std::to_string(table_pos));
        place(robots[i+j], trans, i);
      }
      if(ok_total[i].size()>1 && ok_total[i].size()!=j) table_pos--;
    }
    double dif = ros::Duration( ros::Time::now() - startit).toSec();
    ROS_INFO("%f",dif);
    table_pos++;
  }



  /*

  // take json with positions 
  json j;
  std::ifstream task_json(ros::package::getPath("reachability") + "/task/" + name + ".json");
  task_json >> j;
  task_json.close();

  // all positions for the task
  std::vector<tf2::Vector3> all_positions;
  std::vector<int> obj_positions;
  std::regex reg("\\s+");


  for (auto it = j["start"].begin(); it != j["start"].end(); ++it){
    obj_positions.push_back(std::stoi((*it).get<std::string>()));
  }

  for (auto it = j["positions"].begin(); it != j["positions"].end(); ++it){
    std::string task_string((*it).get<std::string>());
    std::sregex_token_iterator to_it(task_string.begin(), task_string.end(),reg,-1);
    std::vector<std::string> temp{to_it, {}};
    tf2::Vector3 p(std::stof(temp[0]),std::stof(temp[1]),std::stof(temp[2]));
    all_positions.push_back(p);
  }

  

  

  


  // do the chains
  std::vector<std::vector<tf2::Vector3>> task_chains = task_split(all_positions, obj_positions);
  collision_detection::AllowedCollisionMatrix acm = ps.getAllowedCollisionMatrix();

  if(task_chains.size() <= robot_size){
    for(int i = 0; i < task_chains.size(); i++){
      acm.setEntry("object" + std::to_string(i), "panda_" + std::to_string(i+1) + "_leftfinger", true);
      acm.setEntry("object" + std::to_string(i), "panda_" + std::to_string(i+1) + "_rightfinger", true);
    }

    moveit_msgs::PlanningScene pls;
    acm.getMessage(pls.allowed_collision_matrix);
    pls.is_diff = true;
    planning_scene_diff_publisher.publish(pls);


    
    for(int i = 0; i < task_chains.size(); i++){
      int table_pos = obj_positions[i];
      int last = 0;
      for (int j = 0; j < task_chains[i].size(); j++){
        if(last < task_chains[i].size()){
          robots[i].setSupportSurfaceName("table"+ std::to_string(table_pos));
          pick(robots[i], task_chains[i][j],i);

          table_pos++;
          last++;
        
          ;
          place(robots[i], task_chains[i][j+1],i);
        }
      }
    }

    
  }
  */
 
  
  while (ros::ok()){
      ros::spinOnce();
  }
  ros::waitForShutdown();
  return 0;
}