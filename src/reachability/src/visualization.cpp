#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/String.h"
#include <tf/transform_broadcaster.h>
#include "visualization_msgs/InteractiveMarker.h"
#include "visualization_msgs/MarkerArray.h"
#include "math.h" 
#include "moveit/move_group_interface/move_group_interface.h"
#include "moveit/planning_scene_interface/planning_scene_interface.h"
#include <moveit/robot_model_loader/robot_model_loader.h>



std::vector<geometry_msgs::Point> sphere_dec(const float& radius, const int& count, const tf2::Vector3& origin){
  geometry_msgs::Point point;
  std::vector<geometry_msgs::Point> points;

  float alpha, d, m_nu, d_nu, d_phi, nu, m_phi, phi;
  alpha = 4.0f * M_PI * 1 * 1 / count;
  d = sqrt(alpha);
  m_nu = round(M_PI / d);
  d_nu = M_PI / m_nu;
  d_phi = alpha / d_nu;
  for (int i = 0; i < m_nu; i++){
    nu = M_PI * (i + 0.5f) / m_nu;
    m_phi = round(2 * M_PI * sin(nu) / d_phi);
    for(int y = 0; y < m_phi; y++){
      if(radius * cos(nu) + origin.getZ() +0.001 >= origin.getZ()){
        phi = 2 * M_PI * y/m_phi;
        point.x = 1 * sin(nu) * cos(phi) + origin.getX();;
        point.y = 1 * sin(nu) * sin(phi) + origin.getY();
        point.z = 1 * cos(nu) + origin.getZ();
        
        points.push_back(point);
      }
    }
  }

  return points;
}

void cube_dec(const float& abstand, const float& resolution, std::vector<geometry_msgs::Point>& voxels){
  for (float x = 0 - abstand; x <= 0 + abstand; x+= resolution){
      for(float y = 0 - abstand; y <= 0 + abstand; y+= resolution){
          for(float z = 0 - abstand; z <= 0 + abstand; z+= resolution){
              geometry_msgs::Point p;
              p.x =x;
              p.y =y;
              p.z =z;
              voxels.push_back(p);
          }
      }
  }
}


visualization_msgs::Marker sphere_point(tf2::Vector3& origin, double r){
  const double DELTA = M_PI / 10.0f;
  std::vector<visualization_msgs::Marker> p;


  geometry_msgs::Point point;
  std_msgs::ColorRGBA col;
  col.a = 1.0f;
  col.g = 1.0f;
  visualization_msgs::Marker pose;
  pose.header.stamp = ros::Time::now();
  pose.header.frame_id = "world";
  pose.ns = "sp";
  pose.scale.x = 0.03;
  pose.scale.y = 0.03;
  pose.scale.z = 0.03;


  pose.action = visualization_msgs::Marker::ADD;
  pose.type = visualization_msgs::Marker::POINTS;
  pose.lifetime = ros::Duration();
  pose.pose.orientation.w = 1;

  for (double phi = 0; phi < 2*M_PI; phi += DELTA){
    for (double theta = 0; theta <= M_PI; theta += DELTA){
      point.x = 1 * sin(theta) * cos(phi);
      point.y = 1 * sin(theta) * sin(phi);
      point.z = 1 * cos(theta);

      tf2::Quaternion rot;
      rot.setRPY(0, ((M_PI / 2) + theta), phi);
      rot.normalize();

      /*
      visualization_msgs::Marker pose;
      pose.header.frame_id = "world";
      pose.ns = "arrow";
      pose.header.stamp = ros::Time::now();
      pose.action = visualization_msgs::Marker::ADD;
      pose.type = visualization_msgs::Marker::ARROW;
      pose.pose.orientation.w = rot.getW();
      pose.pose.orientation.x = rot.getX();
      pose.pose.orientation.y = rot.getY();
      pose.pose.orientation.z = rot.getZ();
      pose.scale.x =0.05;
      pose.scale.y =0.01;
      pose.scale.z =0.01;
      pose.color.a = pose.color.r = 1;
      pose.pose.position = point;
      pose.id = i;
      p.push_back(pose);
      i++;
      */
      std_msgs::ColorRGBA col;
      col.a =col.g =1;
      if (theta < M_PI *0.25f){
        col.g = 0;
        col.r = 1;
      }

      pose.points.push_back(point);
      pose.colors.push_back(col);

    }
  }

    return pose;
  }



int main(int argc, char **argv){

    float resolution = 0.5f*0.5f*0.5f;
    float max_abstand = 1.25f;
    float res = 1.0f;
    tf2::Vector3 origin(0,0,0);

    ros::init(argc, argv, "visualization");
    ros::NodeHandle n;
    // Load inv_reach and task json file

    ros::Rate loop_rate(1);

    // prepare marker for index
    visualization_msgs::MarkerArray marker_arr;
    visualization_msgs::Marker sphere_points, cube_points;
    marker_arr.markers.push_back(sphere_point(origin, 0.5f));
    ros::Publisher marker_pub = n.advertise< visualization_msgs::MarkerArray>("visualization_marker_array", 1);
    while (ros::ok()){
        marker_pub.publish(marker_arr);
        ros::spinOnce();
    }
}

