#include "../include/map_analyse.h"


void task_sphere(std::vector<tf2::Quaternion>& quat ){
  const double DELTA = M_PI / 4.0f;
  for (double phi = 0; phi < 2*M_PI; phi += DELTA){
    for (double theta = 0; theta <= M_PI*0.5; theta += DELTA){
      tf2::Quaternion rot;
      rot.setRPY(0, ((M_PI / 2) + theta), phi);
      rot.normalize();
      quat.push_back(rot);
    }
  }
}

std::vector<pcl::PointXYZ> BoxTree(const tf2::Vector3& origin, float diameter, float resolution){
  unsigned char depth = 16;
  std::vector<pcl::PointXYZ> box; 
  octomap::OcTree* tree = new octomap::OcTree(resolution/2);
  for (float x = origin.getX() - diameter * 1.5 ; x <= origin.getX() + diameter * 1.5 ; x += resolution){
    for (float y = origin.getY() - diameter * 1.5 ; y <= origin.getY() + diameter * 1.5 ; y += resolution){
      for (float z = origin.getZ() - diameter * 1.5 ; z <= origin.getZ() + diameter * 1.5 ; z += resolution){
        octomap::point3d point(x,y,z);
        tree->updateNode(point, true);
      }
    }
  }

  for (octomap::OcTree::leaf_iterator it = tree->begin_leafs(depth), end = tree->end_leafs(); it != end; ++it){
    pcl::PointXYZ searchPoint(it.getCoordinate().x(), it.getCoordinate().y(), it.getCoordinate().z());
    box.push_back(searchPoint);
  }

  return box;
}

std::vector<pcl::PointXYZ> generateGround(const tf2::Vector3& origin, float diameter, float resolution){
  std::vector<pcl::PointXYZ> ground_plane; 
  resolution/= 2;

  for (float x = origin.getX() - diameter * 1.5; x <= origin.getX() + diameter * 1.5; x += resolution){
    for (float y = origin.getY() - diameter * 1.5; y <= origin.getY() + diameter * 1.5; y += resolution){
      pcl::PointXYZ point(x,y,0);
      ground_plane.push_back(point);     
    }
  }
  return ground_plane;
}

pcl::PointCloud< pcl::PointXYZ >::Ptr vector_to_cloud(std::vector<pcl::PointXYZ>& vector){
  pcl::PointCloud< pcl::PointXYZ >::Ptr task_voxel(new pcl::PointCloud< pcl::PointXYZ >);
  for(pcl::PointXYZ& point : vector)
    task_voxel->push_back(point);
  
  return task_voxel;
}

void chaining(std::vector<std::vector<tf2::Vector3>>& ok, std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total){
  for(std::vector<tf2::Vector3>& tk: ok){
    tf2::Vector3 temp = tk.front();
    std::vector<tf2::Vector3> tk_g;
    tk_g.push_back(temp);

    std::vector<std::vector<tf2::Vector3>> chains; 

    for (int i = 1; i < tk.size(); i++){
      if(tf2::tf2Distance(temp, tk[i])==0){
        chains.push_back(tk_g);
        tk_g.clear();
      }
      tk_g.push_back(tk[i]);
      temp = tk[i];
    }
    chains.push_back(tk_g);
    ok_total.push_back(chains);
  }

  // Debug 
  for(std::vector<std::vector<tf2::Vector3>>& tk: ok_total) for(std::vector<tf2::Vector3>& k : tk) {
    ROS_INFO("-------");
    for(tf2::Vector3& g : k) ROS_INFO("%f %f %f", g.getX(), g.getY(), g.getZ());
  }
}

void marker_per_glied(visualization_msgs::MarkerArray& marker_arr, 
  std::vector<std::vector<std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>>& P_total,
  std::vector<std::vector<std::vector<float>>>& index_total){

  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;

  
  for(int i = 0; i < P_total.size(); i++) for(int j = 0; j < P_total[i].size(); j++) for(int k = 0; k < P_total[i][j].size(); k++){
    marker_template.ns= "Teilkette " + std::to_string(i) + " darin Subkette " + std::to_string(j) + " darin Glied " + std::to_string(k);
    for(int l = 0; l < P_total[i][j][k]->points.size(); l++){
      geometry_msgs::Point p;
      std_msgs::ColorRGBA col;
      col.a = 1;
      float index = index_total[i][j][l];
      col.g = index;

      p.x = P_total[i][j][k]->points[l].x;
      p.y = P_total[i][j][k]->points[l].y;
      p.z = P_total[i][j][k]->points[l].z;
      marker_template.points.push_back(p);
      marker_template.colors.push_back(col);
    }
    marker_arr.markers.push_back(marker_template);
    marker_template.points.clear();
    marker_template.colors.clear();
  }
}

void marker_per_intersection(visualization_msgs::MarkerArray& marker_arr, 
  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, float>>>>& intersections){
  
  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;


  for(int i = 0; i < intersections.size(); i++) for(int j = 0; j < intersections[i].size(); j++){
    marker_template.ns= "Teilkette " + std::to_string(i) + " " + std::to_string(j);
    for(std::pair<pcl::PointXYZ, float>& voxel : intersections[i][j]){
      geometry_msgs::Point p;
      std_msgs::ColorRGBA col;
      col.a = 1;
      col.g = voxel.second;


      p.x = voxel.first.x;
      p.y = voxel.first.y;
      p.z = voxel.first.z;
      marker_template.points.push_back(p);
      marker_template.colors.push_back(col);
    }
    marker_arr.markers.push_back(marker_template);
    marker_template.points.clear();
    marker_template.colors.clear();
  }
}


void marker_intersection_to_ground(visualization_msgs::MarkerArray& marker_arr, 
  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, float>>>>& ground){
  
  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;

  for(int i = 0; i < ground.size(); i++) for(int j = 0; j < ground[i].size(); j++){
    marker_template.ns= "Teilkette auf Grund" + std::to_string(i) + " " + std::to_string(j);
    for(std::pair<pcl::PointXYZ, float>& voxel : ground[i][j]){
      geometry_msgs::Point p;
      std_msgs::ColorRGBA col;
      col.a = 1;
      col.g = voxel.second;


      p.x = voxel.first.x;
      p.y = voxel.first.y;
      p.z = voxel.first.z;
      marker_template.points.push_back(p);
      marker_template.colors.push_back(col);
    }
    marker_arr.markers.push_back(marker_template);
    marker_template.points.clear();
    marker_template.colors.clear();
  }
}

void intersection_filter(std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, float>>>>& intersections,
  std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total, 
  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>>>& voxel_dictionary){

  for(int i = 0; i < voxel_dictionary.size(); i++){
    std::vector<std::vector<std::pair<pcl::PointXYZ, float>>> intersec_per_tk;
    for(int j = 0; j < voxel_dictionary[i].size(); j++){
      std::vector<std::pair<pcl::PointXYZ, float>> intersec_per_subk;
      for(std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>& voxel : voxel_dictionary[i][j]){
        if(voxel.second.size() == ok_total[i][j].size()){
          float index_accumulate = 0;
          for (std::pair<int, float>& index : voxel.second) index_accumulate += index.second;
          index_accumulate/= static_cast<float>(ok_total[i][j].size());
          intersec_per_subk.push_back(std::make_pair(voxel.first, index_accumulate));
        }
      }
      if(!intersec_per_subk.empty()) intersec_per_tk.push_back(intersec_per_subk);
    }
    intersections.push_back(intersec_per_tk);
  }
}

void marker_voxel(visualization_msgs::MarkerArray& marker_arr,
  std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total, 
  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>>>& voxel_dictionary){

  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;

  for(int i = 0; i < voxel_dictionary.size(); i++) for(int j = 0; j < voxel_dictionary[i].size(); j++){
    for(int k = 0; k < ok_total[i][j].size(); k++) {
      marker_template.ns = "Voxelisierung " + std::to_string(i) + " " + std::to_string(j) + " " + std::to_string(k);
      for(std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>& voxel : voxel_dictionary[i][j]) for(std::pair<int, float>& pair : voxel.second)
        if(pair.first == k){
          geometry_msgs::Point point;
          std_msgs::ColorRGBA col;
          point.x = voxel.first.x;
          point.y = voxel.first.y;
          point.z = voxel.first.z;
          col.a = 1;
          col.g = pair.second;
          
          marker_template.points.push_back(point);
          marker_template.colors.push_back(col);
        }
      marker_arr.markers.push_back(marker_template);
      marker_template.points.clear();
      marker_template.colors.clear();
    }
  }
}

void marker_voxel_ground(visualization_msgs::MarkerArray& marker_arr,
  std::vector<std::vector<std::vector<tf2::Vector3>>>& ok_total, 
  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>>>& voxel_dictionary,
  float& resolution,
  std::vector<pcl::PointXYZ>& ground){

  visualization_msgs::Marker marker_template;
  marker_template.header.frame_id = "base_link";
  marker_template.header.stamp = ros::Time::now();
  marker_template.action = visualization_msgs::Marker::ADD;
  marker_template.type = visualization_msgs::Marker::POINTS;
  marker_template.lifetime = ros::Duration();
  marker_template.scale.x = 0.03;
  marker_template.scale.y = 0.03;
  marker_template.scale.z = 0.03;

  for(int i = 0; i < voxel_dictionary.size(); i++) for(int j = 0; j < voxel_dictionary[i].size(); j++){
    for(int k = 0; k < ok_total[i][j].size(); k++) {
      marker_template.ns = "Voxelisierung Boden" + std::to_string(i) + " " + std::to_string(j) + " " + std::to_string(k);
      std::vector<pcl::PointXYZ> points;
      std::vector<float> index;
      for(std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>& voxel : voxel_dictionary[i][j]) for(std::pair<int, float>& pair : voxel.second)
        if(pair.first == k){
          points.push_back(voxel.first);
          index.push_back(pair.second);
        }

      for (pcl::PointXYZ& p : ground){
        pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
        octree.setInputCloud(vector_to_cloud(points));
        octree.addPointsFromInputCloud();
        double min_x, min_y, min_z, max_x, max_y, max_z;
        octree.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
        bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);     
        if (isInBox && octree.isVoxelOccupiedAtPoint(p)) {
          std::vector< int > pointIdxVec;
          if (octree.voxelSearch(p, pointIdxVec)){
            geometry_msgs::Point point;
            std_msgs::ColorRGBA col;
            point.x = p.x;
            point.y = p.y;
            point.z = p.z;
            float index_accumulate = 0;
            for (int& in : pointIdxVec) index_accumulate += index[in];
            index_accumulate /= static_cast<float>(pointIdxVec.size());
            col.a = 1;
            col.g = index_accumulate;
            marker_template.points.push_back(point);
            marker_template.colors.push_back(col);
          }
        }
      }
      marker_arr.markers.push_back(marker_template);
      marker_template.points.clear();
      marker_template.colors.clear();
    }
  }
}

int main(int argc, char **argv){

  float resolution = 0.125f;
  float max_abstand = 2.0f;
  
  std::regex reg("\\s+");


  ros::init(argc, argv, "map_analyse");
  ros::NodeHandle n;
  
  std::vector<float> metrik;
  std::vector<std::string> irm;

  XmlRpc::XmlRpcValue task;
  std::string name;
  std::string rs;


  n.getParam("/Metrik", metrik);
  n.getParam("/Endeffektor_Pose", irm);

  n.getParam("/Aufgabenbeschreibung", task);
  n.getParam("/Name_der_Aufgabe", name);
  n.getParam("/Referenzsystem", rs);



  std::vector<std::vector<tf2::Vector3>> ok;

  for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator it = task.begin(); it != task.end(); ++it){
    std::vector<std::string> tk_s;
    std::vector<tf2::Vector3> tk;

    n.getParam("/Aufgabenbeschreibung/" + it->first, tk_s);
    for (std::string& kg : tk_s){;
      std::sregex_token_iterator to_it(kg.begin(), kg.end(),reg,-1);
      std::vector<std::string> temp{to_it, {}};
      tk.push_back(tf2::Vector3(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2])));
    }
    ok.push_back(tk);
  }

  //Which is coop
  std::vector<std::vector<std::vector<tf2::Vector3>>> ok_total;
  chaining(ok, ok_total);
  
  // How many robots do we need?
  int robot_count = 0;
  for(std::vector<std::vector<tf2::Vector3>>& tk: ok_total) robot_count+= tk.size();
  ROS_INFO("%i", robot_count);


  ros::AsyncSpinner spinner(1); // Never !! change this !
  spinner.start();
  ros::Rate loop_rate(1);

  std::ofstream goal_pos(ros::package::getPath("reachability") + "/maps/position/" + "position.yaml");

  // set a origin point
  tf2::Vector3 p(0,0,0);

  visualization_msgs::MarkerArray marker_arr;
  
  std::vector<std::vector<std::vector<float>>> index_total;
  std::vector<std::vector<std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>> P_total;

  for (std::vector<std::vector<tf2::Vector3>>& tk : ok_total){
    ROS_INFO("---Teilkette---");
    std::vector<std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>> cloud_sub_Teilkette;
    for(std::vector<tf2::Vector3>& sub_tk: tk){
      ROS_INFO("---Subkette---");
      std::vector<std::vector<float>> index_tk;
      std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr> cloud_pro_glied;

      for(tf2::Vector3& kg : sub_tk){
        ROS_INFO("---Kettenglied---");
        pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_task(new pcl::PointCloud< pcl::PointXYZ >);
        std::vector<float> index;
        tf2::Transform task;
        task.setOrigin(kg);
        

        for (int i = 0; i < irm.size(); i++){
          tf2::Transform trans, base;
          std::sregex_token_iterator to_it(irm[i].begin(), irm[i].end(),reg,-1);
          std::vector<std::string> temp{to_it, {}};
          tf2::Vector3 invvec(std::stof(temp[0]), std::stof(temp[1]), std::stof(temp[2]));
          tf2::Quaternion invrot(std::stof(temp[3]), std::stof(temp[4]), std::stof(temp[5]), std::stof(temp[6]));
          
          trans.setOrigin(invvec);
          trans.setRotation(invrot);

          std::vector<tf2::Quaternion> sphere_orientations; 
          task_sphere(sphere_orientations);
          for(tf2::Quaternion& rot: sphere_orientations){
            task.setRotation(rot);
            base = task* trans;

            tf2::Vector3 basevec = base.getOrigin();
            tf2::Quaternion baserot = base.getRotation().normalized();

            pcl::PointXYZ point(basevec.getX(), basevec.getY(), basevec.getZ());
            cloud_task->push_back(point);
            index.push_back(metrik[i]);
          }
        }

        index_tk.push_back(index);
        cloud_pro_glied.push_back(cloud_task);
      }
      index_total.push_back(index_tk);
      cloud_sub_Teilkette.push_back(cloud_pro_glied);
    }
    P_total.push_back(cloud_sub_Teilkette);
  }



  
  int index_count = 0;
  for(std::vector<std::vector<float>>& tk: index_total) index_count+= tk.size();
  ROS_INFO("%i", index_count);

  int cloud_count = 0;
  for(std::vector<std::vector<pcl::PointCloud< pcl::PointXYZ >::Ptr>>& tk: P_total) cloud_count+= tk.size();
  ROS_INFO("Alle P_total pro Kettenglied berechnet.");
  ROS_INFO("Anzahl: %i", cloud_count);

  // prepare grid structures for voxelisation
  std::vector<pcl::PointXYZ> spCenter = BoxTree(p, max_abstand, resolution);
  std::vector<pcl::PointXYZ> grCenter = generateGround(p, max_abstand, resolution);
  ROS_INFO("Arbeitsraum erstellt, starte Voxelisierung.");

  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>>> voxel_dictionary;
  
  ros::Time startit = ros::Time::now();
  for(int i = 0; i < P_total.size();i++){
    ROS_INFO("Teilketten");
    std::vector<std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>>> voxel_teilkette;
    for(int j = 0; j < P_total[i].size();j++){
      ROS_INFO("Subketten");
      std::vector<std::pair<pcl::PointXYZ, std::vector<std::pair<int, float>>>> points_with_index;

      for(pcl::PointXYZ& p : spCenter){
        std::vector<std::pair<int, float>> pair;

        for (int k = 0; k < P_total[i][j].size(); k++){
          pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
          octree.setInputCloud(P_total[i][j][k]);
          octree.addPointsFromInputCloud();
          double min_x, min_y, min_z, max_x, max_y, max_z;
          octree.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
          bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);
            
          if (isInBox && octree.isVoxelOccupiedAtPoint(p)) {
            std::vector< int > pointIdxVec;
            if (octree.voxelSearch(p, pointIdxVec)){
              float index_accumulate = 0;
              for (int& index : pointIdxVec) index_accumulate += index_total[i][j][index];
              index_accumulate/= static_cast<float>(pointIdxVec.size());
              pair.push_back(std::make_pair(k, index_accumulate));
            }
          }
        }
        if(!pair.empty()) points_with_index.push_back(std::make_pair(p, pair));
      }
      ROS_INFO("Entry count %li", points_with_index.size());
      voxel_teilkette.push_back(points_with_index);
    }
    voxel_dictionary.push_back(voxel_teilkette);
  }


  ROS_INFO("Voxelisierung abgeschlossen");

  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, float>>>> intersections;
  intersection_filter(intersections, ok_total, voxel_dictionary);

  ROS_INFO("Schnittmengenanalyse abgeschlossen");
  ROS_INFO("Initialisiere Baseisposition Analyse");

  std::vector<std::vector<std::vector<std::pair<pcl::PointXYZ, float>>>> ground_voxel;
  for(int i = 0; i < intersections.size(); i++){
    std::vector<std::vector<std::pair<pcl::PointXYZ, float>>> voxel_per_subkette;
    for(int j = 0; j < intersections[i].size(); j++){
      
      std::vector<pcl::PointXYZ> points;
      std::vector<float> index;
      for (auto it = std::make_move_iterator(intersections[i][j].begin()), end = std::make_move_iterator(intersections[i][j].end()); it != end; ++it){
        points.push_back(std::move(it->first));
        index.push_back(std::move(it->second));
      }
      ROS_INFO("%li", points.size());

      pcl::PointCloud< pcl::PointXYZ >::Ptr voxeled_task = vector_to_cloud(points);
      pcl::octree::OctreePointCloudSearch< pcl::PointXYZ > groundOctree(resolution);
      groundOctree.setInputCloud(voxeled_task);
      groundOctree.addPointsFromInputCloud();
      std::vector<std::pair<pcl::PointXYZ, float>> pair;

      for(pcl::PointXYZ& p : grCenter){

        double min_x, min_y, min_z, max_x, max_y, max_z;
        groundOctree.getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
        bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);
        
        if (isInBox && groundOctree.isVoxelOccupiedAtPoint(p)) {
          std::vector< int > pointIdxVec;
          if (groundOctree.voxelSearch(p, pointIdxVec)){
            float index_accumulate = 0;
            for (int& in : pointIdxVec) index_accumulate += index[in];
            pair.push_back(std::make_pair(p, index_accumulate/static_cast<float>(pointIdxVec.size())));
          }
        }
      }
      if(!pair.empty()) voxel_per_subkette.push_back(pair);
    }
    ground_voxel.push_back(voxel_per_subkette);
  }
  
  ROS_INFO("Ground filter");
  std::vector<std::vector<std::vector<std::vector<bool>>>> ground_voxel_filter;
  float d = 0.25;
  for(int i = 0; i < ok_total.size(); i++){
    std::vector<std::vector<std::vector<bool>>> filtere_voxel_col;
    for(int j = 0; j < ok_total[i].size(); j++){
      std::vector<std::vector<bool>> filtere_voxel;
      for(std::pair<pcl::PointXYZ, float>& pair : ground_voxel[i][j]){
        std::vector<bool> filtere_voxel_point;
        tf2::Vector3 goal(pair.first.x, pair.first.y, pair.first.z);
        for(int k = 0; k < ok_total[i][j].size(); k++){
          tf2::Vector3 task = ok_total[i][j][k];
          task.setZ(0);
          (tf2::tf2Distance(goal, task) >= d && tf2::tf2Distance(goal, task)) ? filtere_voxel_point.push_back(true) :filtere_voxel_point.push_back(false);
        }
        filtere_voxel.push_back(filtere_voxel_point);
      }
      filtere_voxel_col.push_back(filtere_voxel);
    }
    ground_voxel_filter.push_back(filtere_voxel_col);
  } 


  ROS_INFO("Basis Ermittlung");
  
  std::vector<std::vector<std::pair<pcl::PointXYZ, float>>> goal_tree;
  
  for(int i = 0; i < ground_voxel.size(); i++) { 
    std::vector<std::pair<pcl::PointXYZ, float>> goal_per_sk;
    for(int j = 0; j < ground_voxel[i].size(); j++){
      std::pair<pcl::PointXYZ, float> pair;
      std::vector<pcl::PointXYZ> points;
      std::vector<float> index;
      std::vector<std::pair<pcl::PointXYZ, float>> filter;

      for(int k = 0; k < ground_voxel[i][j].size(); k++){
        if(std::all_of(ground_voxel_filter[i][j][k].begin(), ground_voxel_filter[i][j][k].end(),[](bool v) {return v;})) filter.push_back(ground_voxel[i][j][k]);
      }

      for (auto it = std::make_move_iterator(filter.begin()), end = std::make_move_iterator(filter.end()); it != end; ++it){
        points.push_back(std::move(it->first));
        index.push_back(std::move(it->second));
      }
      int in = std::distance(index.begin(), std::max_element(index.begin(), index.end()));
      pair.first = points[in];
      pair.second = index[in];
      goal_per_sk.push_back(pair);
    }
    goal_tree.push_back(goal_per_sk);
  }
  
  
  int goal_count = 0;
  double dif = ros::Duration( ros::Time::now() - startit).toSec();

  for(std::vector<std::pair<pcl::PointXYZ, float>>& tk : goal_tree) goal_count += tk.size();

  if(goal_count == robot_count){
    ROS_INFO("Positionen für alle %i Roboter kalkuliert", robot_count );
    std::ofstream o(ros::package::getPath("reachability") + "/Positionsanalyse/Base_" + name + ".yaml");
    YAML::Node node;
    node["Referenzsystem"] = rs;
    node["Name_der_Aufgabe"] = name;
    node["Roboter_Anzahl"] = robot_count;
    node["Map_Aufloesung"] = resolution;
    node["Zeit"] = dif;

    int count = 1;
    for(int i = 0; i< goal_tree.size(); i++) for(int j = 0; j< goal_tree[i].size(); j++) {
      for(tf2::Vector3& vec : ok_total[i][j])
        node["Move_Groups"]["objekt" + std::to_string(i)]["panda_arm"+ std::to_string(count)].push_back(std::string(
            std::to_string(vec.getX()) + " " 
          + std::to_string(vec.getY()) + " " 
          + std::to_string(vec.getZ())));
      node["Basispositionen"]["panda_arm"+ std::to_string(count)] = std::string(
        std::to_string(goal_tree[i][j].first.x) + " " 
        +  std::to_string(goal_tree[i][j].first.y) + " " 
        +  std::to_string(goal_tree[i][j].first.z));
      count++;
    }
    o << node;
  }

  marker_per_glied(marker_arr, P_total, index_total);
  marker_voxel(marker_arr, ok_total, voxel_dictionary);
  marker_voxel_ground(marker_arr, ok_total, voxel_dictionary, resolution, grCenter);
  marker_per_intersection(marker_arr, intersections);
  marker_intersection_to_ground(marker_arr, ground_voxel);


  ros::Publisher marker_pub = n.advertise< visualization_msgs::MarkerArray >("visualization_marker_array", 1);
  while (ros::ok()){
      marker_pub.publish(marker_arr);
      ros::spinOnce();
      loop_rate.sleep();
  }


}
